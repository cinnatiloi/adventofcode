This repository has my solution to the puzzles in adventofcode.com.

Folders are divided by year and day.

Each day has a Python3 script that contains my solution.

The code runs on Python 3.8.

After running the script, the solution is printed on the console.
