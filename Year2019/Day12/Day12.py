import copy
import math
import os


class Day12(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile(self):
        # type: () -> list[list[int]]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                curPos = [0] * 3
                index = 0
                for i in range(len(curPos)):
                    equalIndex = strippedLine.find("=", index)
                    commaIndex = strippedLine.find(",", index)
                    arrowIndex = strippedLine.find(">", index)
                    startIndex = equalIndex+1
                    endIndex = commaIndex if commaIndex >= 0 else arrowIndex
                    curVal = int(strippedLine[startIndex:endIndex])
                    curPos[i] = curVal
                    index = endIndex + 1
                retList.append(curPos)
        return retList

    def solve1(self, steps):
        # type: (int) -> int
        posList = self.parseFile()
        # convert to list indexed by coordinate
        posListByCoord = []
        for i in range(len(posList[0])):
            newPosList = []
            for pos in posList:
                newPosList.append(pos[i])
            posListByCoord.append(newPosList)
        velListByCoord = [[0] * len(posListByCoord[0]) for _ in range(len(posListByCoord))]

        for _ in range(steps):
            for i in range(3):
                self.updateVelocity(posListByCoord, velListByCoord, i)
                self.updatePosition(posListByCoord, velListByCoord, i)

        return self.calculateEnergy(posListByCoord, velListByCoord)

    def solve2(self):
        # type: () -> int
        posList = self.parseFile()
        # convert to list indexed by coordinate
        posListByCoord = []
        for i in range(len(posList[0])):
            newPosList = []
            for pos in posList:
                newPosList.append(pos[i])
            posListByCoord.append(newPosList)
        velListByCoord = [[0] * len(posListByCoord[0]) for _ in range(len(posListByCoord))]

        initPosListByCoord = copy.deepcopy(posListByCoord)
        initVelListByCoord = copy.deepcopy(velListByCoord)

        countList = []
        for i in range(len(initPosListByCoord)):
            count = 0
            while True:
                self.updateVelocity(posListByCoord, velListByCoord, i)
                self.updatePosition(posListByCoord, velListByCoord, i)
                count += 1
                if (initPosListByCoord[i] == posListByCoord[i]) and (initVelListByCoord[i] == velListByCoord[i]):
                    break
            countList.append(count)

        return self.calculateLCM(countList)

    @staticmethod
    def updateVelocity(posList, velList, i):
        # type: (list[list[int]], list[list[int]], int) -> None
        for idx1, (refPos, refVel) in enumerate(zip(posList[i], velList[i])):
            for idx2, cmpPos in enumerate(posList[i]):
                # skip same position
                if idx1 == idx2:
                    continue
                if refPos < cmpPos:
                    velList[i][idx1] += 1
                elif refPos > cmpPos:
                    velList[i][idx1] -= 1

    @staticmethod
    def updatePosition(posList, velList, i):
        # type: (list[list[int]], list[list[int]], int) -> None
        for idx, vel in enumerate(velList[i]):
            posList[i][idx] += vel

    @staticmethod
    def calculateEnergy(posList, velList):
        # type: (list[list[int]], list[list[int]]) -> int
        potList = [0] * len(posList[0])
        kinList = [0] * len(posList[0])
        for pos, vel in zip(posList, velList):
            for i, x in enumerate(pos):
                potList[i] += abs(x)
            for i, x in enumerate(vel):
                kinList[i] += abs(x)
        total = 0
        for pot, kin in zip(potList, kinList):
            total += pot * kin
        return total

    @staticmethod
    def calculateLCM(a):
        # type: (list[int]) -> int
        lcm = a[0]
        for i in a[1:]:
            lcm = lcm * i // math.gcd(lcm, i)
        return lcm


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    for i, steps in enumerate([10, 100]):
        uut = Day12(curDir + "/input_short_{}.txt".format(i))
        print("Sample {} Part 1: {}".format(i, uut.solve1(steps)))
        print("Sample {} Part 2: {}".format(i, uut.solve2()))
    uut = Day12(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1(1000)))
    print("Solution Part 2: {}".format(uut.solve2()))
