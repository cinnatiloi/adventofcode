import os


class Day04(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename

    def parseFile(self):
        # type: () -> list[list[int,int]]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                retList.append(line.split("-"))
        return retList

    def solve1(self):
        # type: () -> list[int]
        retList = self.parseFile()

        validList = []
        for r in retList:
            minVal = int(r[0])
            maxVal = int(r[1])
            numValid = 0
            for val in range(minVal,maxVal+1):
                if self.isValid(val):
                    numValid += 1
            validList.append(numValid)

        return validList

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        validList = []
        for r in retList:
            minVal = int(r[0])
            maxVal = int(r[1])
            numValid = 0
            for val in range(minVal,maxVal+1):
                if self.isAdvancedValid(val):
                    numValid += 1
            validList.append(numValid)

        return validList

    @staticmethod
    def isValid(val):
        # type: (int) -> bool
        valStr = "{0:06}".format(val)
        isIncreasing = True
        hasDouble = False
        for i in range(1, len(valStr)):
            if valStr[i] == valStr[i-1]:
                hasDouble = True
            if valStr[i] < valStr[i-1]:
                isIncreasing = False
        return isIncreasing and hasDouble

    @staticmethod
    def isAdvancedValid(val):
        # type: (int) -> bool
        valStr = "{0:06}".format(val)
        isIncreasing = True
        for i in range(1, len(valStr)):
            if valStr[i] < valStr[i - 1]:
                isIncreasing = False
        if not isIncreasing:
            return False
        consecutiveCount = {}
        for i in range(len(valStr)):
            curDigit = valStr[i]
            if consecutiveCount.get(curDigit) is None:
                consecutiveCount[curDigit] = 1
            count = 1
            for j in range(i-1,-1,-1):
                testDigit = valStr[j]
                if curDigit != testDigit:
                    break
                count += 1
            if consecutiveCount[curDigit] < count:
                consecutiveCount[curDigit] = count
        for count in consecutiveCount.values():
            if count == 2:
                return True
        return False

    @staticmethod
    def printIsValid(val):
        # type: (int) -> None
        print("{} {} valid in part 1".format(val, "is" if Day04.isValid(val) else "is not"))

    @staticmethod
    def printIsAdvancedValid(val):
        # type: (int) -> None
        print("{} {} valid in part 2".format(val, "is" if Day04.isAdvancedValid(val) else "is not"))


if __name__ == "__main__":
    Day04.printIsValid(111111)
    Day04.printIsValid(223450)
    Day04.printIsValid(123789)
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day04(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    Day04.printIsAdvancedValid(112233)
    Day04.printIsAdvancedValid(123444)
    Day04.printIsAdvancedValid(111122)
    print("Solution Part 2: {}".format(uut.solve2()))
