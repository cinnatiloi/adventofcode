import os


class Day16(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile(self):
        # type: () -> list[int]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                retList = list(strippedLine)
                retList = [int(i) for i in retList]
        return retList

    def solve1(self):
        # type: () -> list[int]
        retList = self.parseFile()

        retList = self.runFFT(retList, 1)

        return retList[:8]

    def solve2(self):
        # type: () -> list[int]
        retList = self.parseFile()

        skipNum = int("".join(map(str, retList[:7])))

        retList = self.runFFT(retList, 10000)

        return retList[skipNum:skipNum+8]

    def runFFT(self, retList, repetition):
        # type: (list[int], int) -> list[int]
        numPhases = 100
        for phaseIdx in range(numPhases):
            newList = []
            for idx1 in range(len(retList)):
                isAdd = True
                idx2 = idx1
                total = 0
                while idx2 < len(retList):
                    val = sum(retList[idx2:idx2+idx1+1])
                    if isAdd:
                        total += val
                    else:
                        total -= val
                    idx2 += 2*(idx1+1)
                    isAdd = not isAdd
                newList.append(abs(total) % 10)
            retList = newList
        return retList


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    for idx in range(4):
        uut = Day16(curDir + "/input_short_{}.txt".format(idx))
        print("Sample {} Part 1: {}".format(idx, uut.solve1()))
    #for idx in range(4, 7):
    #    uut = Day16(curDir + "/input_short_{}.txt".format(idx))
    #    print("Sample {} Part 2: {}".format(idx, uut.solve2()))
    uut = Day16(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    #print("Solution Part 2: {}".format(uut.solve2()))
