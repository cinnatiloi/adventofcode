import os
from typing import Optional

from Year2019.IntcodeComputer.IntcodeComputer import IntcodeComputer, IntcodeComputerState_e


class Day13(object):

    TILE_EMPTY = 0
    TILE_WALL = 1
    TILE_BLOCK = 2
    TILE_HOR_PADDLE = 3
    TILE_BALL = 4

    CHAR_MAP_DICT = {
        TILE_EMPTY: " ",
        TILE_WALL: "#",
        TILE_BLOCK: "H",
        TILE_HOR_PADDLE: "-",
        TILE_BALL: "o"
    }

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile(self):
        # type: () -> list[int]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                curList = line.split(",")
                curList = [int(x) for x in curList]
                retList += curList
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        (screenDict, _) = self.runProgram(retList)

        count = 0
        for loc, tile in screenDict.items():
            if tile == self.TILE_BLOCK:
                count += 1

        return count

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()
        retList[0] = 2

        (_, userScore) = self.runProgram(retList)

        return userScore

    def runProgram(self, retList):
        # type: (list[int]) -> tuple[dict[tuple[int, int], int], Optional[int]]

        # set manualRun to True to play manually
        manualRun = False
        # set printBoard to True to see the game board
        printBoard = False

        if manualRun:
            askForInput = True
            breakOnInput = False
        else:
            askForInput = False
            breakOnInput = True

        computer = IntcodeComputer(retList)
        screenDict = {}
        userScore = None
        maxX = None
        maxY = None
        ballX = None
        paddleX = None
        while True:
            state = IntcodeComputerState_e.NOT_DONE
            computer.clearOutputList()
            # print userScore and screen
            if printBoard:
                self.printState(screenDict, maxX, maxY, userScore)
            for _ in range(3):
                while True:
                    state = computer.run(breakOnOutput=True, askForInput=askForInput, breakOnInput=breakOnInput)
                    if state in [IntcodeComputerState_e.DONE, IntcodeComputerState_e.BREAK_ON_OUTPUT]:
                        break
                    if (ballX is None) or (paddleX is None):
                        break
                    if ballX > paddleX:
                        inputVal = 1
                    elif ballX < paddleX:
                        inputVal = -1
                    else:
                        inputVal = 0
                    computer.clearInput()
                    computer.addInput(inputVal)
                if state == IntcodeComputerState_e.DONE:
                    break
            if state == IntcodeComputerState_e.DONE:
                break
            outputList = computer.getOutputList()
            x = outputList[0]
            y = outputList[1]
            tile = outputList[2]
            if x == -1 and y == 0:
                userScore = tile
            else:
                if (maxX is None) or (x > maxX):
                    maxX = x
                if (maxY is None) or (y > maxY):
                    maxY = y
                if tile == self.TILE_BALL:
                    ballX = x
                if tile == self.TILE_HOR_PADDLE:
                    paddleX = x
                screenDict[(x, y)] = tile

        return screenDict, userScore

    def printState(self, screenDict, maxX, maxY, userScore):
        # type: (dict[tuple[int, int], int], int, int, int) -> None
        if (maxX is None) or (maxY is None) or (userScore is None):
            return
        print("Current Score: {}".format(userScore))
        print("Current Board:")
        for y in range(maxY):
            curRow = ""
            for x in range(maxX):
                curTile = screenDict.get((x, y))
                if curTile is None:
                    curRow += self.CHAR_EMPTY
                else:
                    curRow += self.CHAR_MAP_DICT[curTile]
            print(curRow)


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day13(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
