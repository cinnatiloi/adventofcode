import copy
import os
from typing import Optional

from Year2019.IntcodeComputer.IntcodeComputer import IntcodeComputer


class Day02(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename

    def parseFile(self):
        # type: () -> list[int]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                curList = line.split(",")
                curList = [int(x) for x in curList]
                retList += curList
        return retList

    def solveShort(self):
        # type: () -> int
        retList = self.parseFile()

        return self.runProgram(retList, None, None)

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        return self.runProgram(retList, 12, 2)

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        for noun in range(100):
            for verb in range(100):
                curList = copy.copy(retList)
                if self.runProgram(curList, noun, verb) == 19690720:
                    return 100 * noun + verb
        return None

    @staticmethod
    def runProgram(retList, noun, verb):
        # type: (list[int], Optional[int], Optional[int]) -> int

        # initial replace
        if noun is not None:
            retList[1] = noun
        if verb is not None:
            retList[2] = verb

        computer = IntcodeComputer(retList)
        computer.run()
        return computer.getInstrList()[0]


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day02(curDir + "/input_short.txt")
    print("Sample: {}".format(uut.solveShort()))
    uut = Day02(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
