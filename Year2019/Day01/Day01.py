import os


class Day01(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename

    def parseFile(self):
        # type: () -> list[int]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                retList.append(int(line))
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        total = 0
        for val in retList:
            total += self.calcFuel(val)
        return total

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        total = 0
        for val in retList:
            val = self.calcFuel(val)
            total += val
            while val > 0:
                val = self.calcFuel(val)
                total += val

        return total

    @staticmethod
    def calcFuel(mass):
        # type: (int) -> int
        fuel = int(mass / 3)
        fuel -= 2
        if fuel < 0:
            fuel = 0
        return fuel


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day01(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day01(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
