import os

from Year2019.IntcodeComputer.IntcodeComputer import IntcodeComputer, IntcodeComputerState_e


class Day11(object):

    UP = 0
    DOWN = 1
    LEFT = 2
    RIGHT = 3

    BLACK = 0
    WHITE = 1

    TURN_LEFT = 0
    TURN_RIGHT = 1

    WHITE_CHAR = "*"
    BLACK_CHAR = " "

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile(self):
        # type: () -> list[int]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                curList = line.split(",")
                curList = [int(x) for x in curList]
                retList += curList
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        mapDict = self.runPaintRobot(retList, self.BLACK)
        return len(mapDict.keys())

    def solve2(self):
        # type: () -> str
        retList = self.parseFile()
        mapDict = self.runPaintRobot(retList, self.WHITE)

        maxX = None
        maxY = None
        minX = None
        minY = None
        for curPoint in mapDict.keys():
            curX = curPoint[0]
            curY = curPoint[1]
            if (maxX is None) or (curX > maxX):
                maxX = curX
            if (minX is None) or (curX < minX):
                minX = curX
            if (maxY is None) or (curY > maxY):
                maxY = curY
            if (minY is None) or (curY < minY):
                minY = curY

        retStr = os.linesep
        for j in range(maxY, minY-1, -1):
            for i in range(minX, maxX+1):
                curPoint = (i, j)
                curColor = mapDict.get(curPoint)
                if (curColor is None) or (curColor == self.BLACK):
                    retStr += self.BLACK_CHAR
                elif curColor == self.WHITE:
                    retStr += self.WHITE_CHAR
                else:
                    raise ValueError("Unknown color {}".format(curColor))
            retStr += os.linesep

        return retStr

    def runPaintRobot(self, retList, initialColor):
        # type: (list[int], int) -> dict[tuple[int, int], int]
        computer = IntcodeComputer(retList)
        curPoint = (0, 0)
        curDirection = self.UP
        curColor = initialColor

        mapDict = {}

        numOutputsPerRun = 2

        while True:
            computer.clearInput()
            computer.addInput(curColor)
            for _ in range(numOutputsPerRun):
                state = computer.run(breakOnOutput=True)
                if state == IntcodeComputerState_e.DONE:
                    break
            if state == IntcodeComputerState_e.DONE:
                break
            paintColor = computer.getOutputList()[0]
            turn = computer.getOutputList()[1]
            computer.clearOutputList()
            mapDict[curPoint] = paintColor
            (curDirection, curPoint) = self.updatePoint(curDirection, curPoint, turn)
            curColor = mapDict.get(curPoint)
            if curColor is None:
                curColor = self.BLACK

        return mapDict

    def updatePoint(self, curDirection, curPoint, turn):
        # type: (int, tuple[int, int], int) -> tuple[int, tuple[int, int]]
        if curDirection == self.UP:
            if turn == self.TURN_LEFT:
                return self.LEFT, (curPoint[0]-1, curPoint[1])
            elif turn == self.TURN_RIGHT:
                return self.RIGHT, (curPoint[0]+1, curPoint[1])
            else:
                raise ValueError("Unknown turn {}".format(turn))
        elif curDirection == self.LEFT:
            if turn == self.TURN_LEFT:
                return self.DOWN, (curPoint[0], curPoint[1]-1)
            elif turn == self.TURN_RIGHT:
                return self.UP, (curPoint[0], curPoint[1]+1)
            else:
                raise ValueError("Unknown turn {}".format(turn))
        elif curDirection == self.DOWN:
            if turn == self.TURN_LEFT:
                return self.RIGHT, (curPoint[0]+1, curPoint[1])
            elif turn == self.TURN_RIGHT:
                return self.LEFT, (curPoint[0]-1, curPoint[1])
            else:
                raise ValueError("Unknown turn {}".format(turn))
        elif curDirection == self.RIGHT:
            if turn == self.TURN_LEFT:
                return self.UP, (curPoint[0], curPoint[1]+1)
            elif turn == self.TURN_RIGHT:
                return self.DOWN, (curPoint[0], curPoint[1]-1)
            else:
                raise ValueError("Unknown turn {}".format(turn))
        else:
            raise ValueError("Unknown current direction {}".format(curDirection))


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day11(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
