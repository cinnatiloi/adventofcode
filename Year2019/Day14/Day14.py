import math
import os
from collections import defaultdict
from typing import Union


class Day14(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile(self):
        # type: () -> dict[str, dict[str, Union[int, dict[str, int]]]]
        retDict = {}
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                fields = strippedLine.split('=>')
                outputFields = fields[1].strip().split(" ")
                key = outputFields[1].strip()
                retDict[key] = {}
                count = int(outputFields[0])
                retDict[key]['outputCount'] = count
                retDict[key]['inputDict'] = {}
                inputList = fields[0].strip().split(",")
                for inputVal in inputList:
                    fields = inputVal.strip().split(" ")
                    retDict[key]['inputDict'][fields[1]] = int(fields[0])
        return retDict

    def solve1(self):
        # type: () -> int
        retDict = self.parseFile()

        count = self.getTargetAmount(retDict, 1)

        return count

    def solve2(self):
        # type: () -> int
        retDict = self.parseFile()

        targetOre = 10**12
        incrSteps = 2
        numFuel = 1

        foundInitial = False
        while True:
            oreCount = self.getTargetAmount(retDict, numFuel)
            #print(foundInitial)
            #print(numFuel)
            #print(oreCount)
            #print(incrSteps)
            if oreCount == targetOre:
                break
            if oreCount < targetOre:
                if foundInitial:
                    if incrSteps > 1:
                        numFuel += incrSteps
                        incrSteps //= 2
                        numFuel -= incrSteps
                    else:
                        break
                else:
                    numFuel *= 2
            elif oreCount > targetOre:
                if not foundInitial:
                    foundInitial = True
                    incrSteps = numFuel // 2
                numFuel -= incrSteps

        return numFuel

    def getTargetAmount(self, retDict, numFuel):
        # type: (dict[str, dict[str, Union[int, dict[str, int]]]], int) -> int

        start = "FUEL"
        target = "ORE"

        countDict = defaultdict(int)
        countDict[start] = numFuel

        extraDict = defaultdict(int)
        #print("countDict", countDict)
        #print("extraDict", extraDict)

        while not ((target in countDict) and (len(countDict) == 1)):
            newCountDict = defaultdict(int)
            for material in extraDict.keys():
                if extraDict[material] >= countDict[material]:
                    extraDict[material] -= countDict[material]
                    countDict.pop(material)
            for material in countDict.keys():
                if material == target:
                    newCountDict[material] += countDict[material]
                    continue
                infoDict = retDict[material]
                extra = 0
                if material in extraDict:
                    extra = extraDict[material]
                numChange = math.ceil((countDict[material] - extra) / infoDict['outputCount'])
                totalChange = numChange * infoDict['outputCount']
                for inputMat, inputCount in infoDict['inputDict'].items():
                    newCountDict[inputMat] += inputCount * numChange
                if countDict[material] > totalChange:
                    newCountDict[material] += countDict[material] - totalChange
                elif totalChange > countDict[material]:
                    extraDict[material] += totalChange - countDict[material]
            countDict = newCountDict
            #print("countDict", countDict)
            #print("extraDict", extraDict)

        return countDict[target]


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    for i in range(0,5):
        uut = Day14(curDir + "/input_short_{}.txt".format(i))
        print("Sample {} Part 1: {}".format(i, uut.solve1()))
        print("Sample {} Part 2: {}".format(i, uut.solve2()))
    uut = Day14(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
