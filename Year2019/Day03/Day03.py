import os


class Day03(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename

    def parseFile(self):
        # type: () -> list[list[str]]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                retList.append(line.split(","))
        return retList

    def solve1(self):
        # type: () -> list[int]
        retList = self.parseFile()

        index = 0
        minDistList =[]
        while index < len(retList):
            path0 = self.getPath(retList[index])
            path1 = self.getPath(retList[index+1])
            # find intersecting points
            intersection = self.getIntersection(path0, path1)
            # find minimum distance
            minDist = None
            for p in intersection:
                dist = abs(p[0]) + abs(p[1])
                if (minDist is None) or (dist < minDist):
                    minDist = dist
            minDistList.append(minDist)
            index += 2

        return minDistList

    def solve2(self):
        # type: () -> list[int]
        retList = self.parseFile()

        index = 0
        minDistList =[]
        while index < len(retList):
            path0 = self.getPath(retList[index])
            path1 = self.getPath(retList[index+1])
            # find intersecting points
            intersection = self.getIntersection(path0, path1)
            # find minimum steps
            minSteps = None
            for p in intersection:
                steps = path0[p] + path1[p]
                if (minSteps is None) or (steps < minSteps):
                    minSteps = steps
            minDistList.append(minSteps)
            index += 2

        return minDistList

    @staticmethod
    def getPath(cmdList):
        # type: (list[str]) -> dict[tuple[int,int],int]
        retPath = {}
        curPos = (0, 0)

        totalSteps = 0
        for cmd in cmdList:
            direction = cmd[0]
            steps = int(cmd[1:])
            if direction == "R":
                increment = 1
                index = 0
            elif direction == "L":
                increment = -1
                index = 0
            elif direction == "U":
                increment = 1
                index = 1
            elif direction == "D":
                increment = -1
                index = 1
            else:
                raise ValueError("Unknown direction {}".format(direction))
            for i in range(steps):
                if index == 0:
                    curPos = (curPos[0] + increment, curPos[1])
                else:
                    curPos = (curPos[0], curPos[1] + increment)
                totalSteps += 1
                retPath[curPos] = totalSteps
        return retPath


    @staticmethod
    def getIntersection(path0, path1):
        intersection = []
        for p in path1:
            if path0.get(p) is not None:
                intersection.append(p)
        return intersection


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day03(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day03(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
