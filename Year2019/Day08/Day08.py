import os


class Day08(object):

    BLACK = '0'
    WHITE = '1'
    TRANSPARENT = '2'

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str
        self.numRows = 0  # type: int
        self.numCols = 0  # type: int

    def parseFile(self):
        # type: () -> str
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                return strippedLine

    def setDimensions(self, numRows, numCols):
        # type: (int, int) -> None
        self.numRows = numRows
        self.numCols = numCols

    def solve1(self):
        # type: () -> int
        retStr = self.parseFile()
        layersList = self.buildLayers(retStr)

        min0 = None
        min1 = None
        min2 = None
        for layer in layersList:
            num0 = 0
            num1 = 0
            num2 = 0
            for row in layer:
                num0 += row.count('0')
                num1 += row.count('1')
                num2 += row.count('2')
            if (min0 is None) or (min0 > num0):
                min0 = num0
                min1 = num1
                min2 = num2

        return min1 * min2

    def solve2(self):
        # type: () -> str
        retStr = self.parseFile()
        layersList = self.buildLayers(retStr)

        numLayers = len(layersList)
        image = []
        for i in range(self.numRows):
            imageRow = ""
            for j in range(self.numCols):
                imagePixel = " "
                for layerIdx in range(numLayers):
                    curPixel = layersList[layerIdx][i][j]
                    if curPixel == self.BLACK:
                        imagePixel = " "
                        break
                    elif curPixel == self.WHITE:
                        imagePixel = "*"
                        break
                imageRow += imagePixel
            image.append(imageRow)

        imageStr = os.linesep
        for imageRow in image:
            imageStr += imageRow + os.linesep

        return imageStr

    def buildLayers(self, retStr):
        # type: (str) -> list[list[str]]
        if self.numRows == 0 or self.numCols == 0:
            raise ValueError("Dimensions must be non-zero ({}, {})".format(self.numRows, self.numCols))
        layersList = []
        charIdx = 0
        while charIdx < len(retStr):
            layer = []
            for i in range(self.numRows):
                curRow = retStr[charIdx:charIdx+self.numCols]
                layer.append(curRow)
                charIdx += self.numCols
            layersList.append(layer)
        return layersList


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day08(curDir + "/input_short_0.txt")
    uut.setDimensions(2, 3)
    print("Sample Part 1: {}".format(uut.solve1()))
    uut = Day08(curDir + "/input_short_1.txt")
    uut.setDimensions(2, 2)
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day08(curDir + "/input.txt")
    uut.setDimensions(6, 25)
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
