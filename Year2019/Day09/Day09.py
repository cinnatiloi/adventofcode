import os

from Year2019.IntcodeComputer.IntcodeComputer import IntcodeComputer


class Day09(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename

    def parseFile(self):
        # type: () -> list[int]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                curList = line.split(",")
                curList = [int(x) for x in curList]
                retList += curList
        return retList

    def solve1(self):
        # type: () -> list[int]
        retList = self.parseFile()

        computer = IntcodeComputer(retList)
        computer.addInput(1)
        computer.run()

        return computer.getOutputList()

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        computer = IntcodeComputer(retList)
        computer.addInput(2)
        computer.run()

        return computer.getOutputList()


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    for idx in range(3):
        uut = Day09(curDir + "/input_short_{}.txt".format(idx))
        print("Sample {} Part 1: {}".format(idx, uut.solve1()))
    uut = Day09(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
