import itertools
import os

from Year2019.IntcodeComputer.IntcodeComputer import IntcodeComputer, IntcodeComputerState_e


class Day07(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename

    def parseFile(self):
        # type: () -> list[int]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                curList = line.split(",")
                curList = [int(x) for x in curList]
                retList += curList
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        maxThruster = 0
        maxPhaseList = []
        testList = list(itertools.permutations(range(5)))
        for phaseList in testList:
            curThruster = 0
            for phase in phaseList:
                computer = IntcodeComputer(retList)
                computer.addInput(phase)
                computer.addInput(curThruster)
                computer.run()
                curThruster = computer.getOutputList()[0]
            if maxThruster < curThruster:
                maxThruster = curThruster
                maxPhaseList = phaseList

        print("Max phase list: {}".format(maxPhaseList))
        return maxThruster

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        maxThruster = 0
        maxPhaseList = []
        testList = list(itertools.permutations(range(5, 10)))
        for phaseList in testList:
            curThruster = 0
            numPhases = len(phaseList)
            phaseIdxList = range(numPhases)
            computerList = []
            for phase in phaseList:
                computer = IntcodeComputer(retList)
                computer.addInput(phase)
                computerList.append(computer)
            computerStateList = [IntcodeComputerState_e.NOT_DONE] * numPhases
            while computerStateList != ([IntcodeComputerState_e.DONE] * numPhases):
                for phaseIdx, phase, computer in zip(phaseIdxList, phaseList, computerList):
                    computer.addInput(curThruster)
                    state = computer.run(breakOnOutput=True)
                    if state != IntcodeComputerState_e.DONE:
                        curThruster = computer.getOutputList()[0]
                        computer.clearInput()
                        computer.clearOutputList()
                    computerStateList[phaseIdx] = state
            if maxThruster < curThruster:
                maxThruster = curThruster
                maxPhaseList = phaseList

        print("Max phase list: {}".format(maxPhaseList))
        return maxThruster


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    for idx in range(3):
        uut = Day07(curDir + "/input_short_{}.txt".format(idx))
        print("Sample {} Part 1: {}".format(idx, uut.solve1()))
    for idx in range(3,5):
        uut = Day07(curDir + "/input_short_{}.txt".format(idx))
        print("Sample {} Part 2: {}".format(idx, uut.solve2()))
    uut = Day07(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
