import os

from Year2019.IntcodeComputer.IntcodeComputer import IntcodeComputer


class Day05(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename

    def parseFile(self):
        # type: () -> list[int]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                curList = line.split(",")
                curList = [int(x) for x in curList]
                retList += curList
        return retList

    def solve1(self):
        # type: () -> list[int]
        retList = self.parseFile()
        return self.runProgram(retList, 1)

    def solve2(self):
        # type: () -> list[int]
        retList = self.parseFile()
        return self.runProgram(retList, 5)

    @staticmethod
    def runProgram(retList, inputVal):
        # type: (list[int], int) -> int
        computer = IntcodeComputer(retList)
        computer.addInput(inputVal)
        computer.run()
        return computer.getOutputList()


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day05(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
