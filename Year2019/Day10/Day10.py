import math
import os


class Day10(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile(self):
        # type: () -> list[str]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                retList.append(strippedLine)
        return retList

    def solve1(self):
        # type: () -> tuple[int, tuple[int, int], list[tuple[int, int]]]
        retList = self.parseFile()
        asteroidLocList = self.getAsteroidLocList(retList)

        bestLocation = None
        maxDetected = 0
        for asteroidLoc in asteroidLocList:
            distanceSet = set()
            for candidateLoc in asteroidLocList:
                # skip itself
                if asteroidLoc == candidateLoc:
                    continue
                distance = (candidateLoc[0] - asteroidLoc[0], candidateLoc[1] - asteroidLoc[1])
                distGcd = math.gcd(distance[0], distance[1])
                lowDistance = (int(distance[0]/distGcd), int(distance[1]/distGcd))
                distanceSet.add(lowDistance)
            if len(distanceSet) > maxDetected:
                maxDetected = len(distanceSet)
                bestLocation = asteroidLoc

        return maxDetected, bestLocation, asteroidLocList

    def solve2(self):
        # type: () -> tuple[int, tuple[int, int]]
        (_, bestLocation, asteroidLocList) = self.solve1()

        polarDict = {}
        for asteroidLoc in asteroidLocList:
            distance = (asteroidLoc[0] - bestLocation[0], bestLocation[1] - asteroidLoc[1])
            phase = math.atan2(distance[0], distance[1])
            if phase < 0:
                phase += 2 * math.pi
            magnitude = math.sqrt(distance[0]**2 + distance[1]**2)
            if polarDict.get(phase) is None:
                polarDict[phase] = []
            polarDict[phase].append(magnitude)

        # sort magnitudes
        for phase in polarDict.keys():
            polarDict[phase].sort()

        sortedPhaseList = sorted(polarDict.keys())
        addedItem = True
        vaporizeList = []
        while addedItem:
            addedItem = False
            for phase in sortedPhaseList:
                if polarDict[phase]:
                    magnitude = polarDict[phase][0]
                    y = round(magnitude * math.cos(phase))
                    x = round(magnitude * math.sin(phase))
                    asteroidLoc = (bestLocation[0] + x, bestLocation[1] - y)
                    vaporizeList.append(asteroidLoc)
                    polarDict[phase] = polarDict[phase][1:]
                    addedItem = True

        if len(vaporizeList) <= 200:
            # use last item
            item200 = vaporizeList[-1]
        else:
            item200 = vaporizeList[199]

        val = item200[0] * 100 + item200[1]

        return val, item200

    @staticmethod
    def getAsteroidLocList(retList):
        # type: (list[str]) -> list[tuple[int, int]]
        asteroidLocList = []
        for i, line in enumerate(retList):
            for j, c in enumerate(line):
                if c == "#":
                    asteroidLocList.append((j, i))
        return asteroidLocList


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    for i in range(5):
        uut = Day10(curDir + "/input_short_{}.txt".format(i))
        (maxDetected, bestLocation, _) = uut.solve1()
        print("Sample {} Part 1: {} at {}".format(i, maxDetected, bestLocation))
    for i in range(5):
        uut = Day10(curDir + "/input_short_{}.txt".format(i))
        (val, loc) = uut.solve2()
        print("Sample {} Part 2: {} at {}".format(i, val, loc))
    uut = Day10(curDir + "/input.txt")
    (maxDetected, bestLocation, _) = uut.solve1()
    print("Solution Part 1: {} at {}".format(maxDetected, bestLocation))
    (val, loc) = uut.solve2()
    print("Solution Part 2: {} at {}".format(val, loc))
