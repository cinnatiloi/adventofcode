import os


class MyTree(object):

    def __init__(self, retList):
        # type: (list[list[str]]) -> None
        self.connectionDict = {} # type: dict[str, list[str]]
        self.weightDict = {} # type: dict[str, int]
        self.parentDict = {} # type: dict[str, str]
        self.build(retList)

    def build(self, retList):
        # type: (list[list[str]]) -> None

        self.connectionDict = {}
        self.weightDict = {}
        self.parentDict = {}

        for connection in retList:
            parent = connection[0]
            child = connection[1]
            # initialize dictionaries for parent
            if self.connectionDict.get(parent) is None:
                self.connectionDict[parent] = []
            if self.weightDict.get(parent) is None:
                self.weightDict[parent] = 0
            self.parentDict[child] = parent
            # add child to connection
            self.connectionDict[parent].append(child)
            # increment all children's weights
            self.incrementWeight(self.weightDict[parent], self.connectionDict[parent])

    def incrementWeight(self, parentWeight, childList):
        # type: (int, list[str]) -> None
        for child in childList:
            self.weightDict[child] = parentWeight + 1
            # increment all children's weights
            if self.connectionDict.get(child) is not None:
                self.incrementWeight(self.weightDict[child], self.connectionDict[child])

    def getWeightDict(self):
        # type: () -> dict[str, int]
        return self.weightDict

    def getConnectionDict(self):
        # type: () -> dict[str, list[str]]
        return self.connectionDict

    def getParentList(self, child):
        # type: (str) -> list[str]
        parentList = []
        curChild = self.parentDict.get(child)
        while curChild is not None:
            parentList.insert(0, curChild)
            curChild = self.parentDict.get(curChild)
        return parentList


class Day06(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename

    def parseFile(self):
        # type: () -> list[list[str]]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                retList.append(line.strip().split(")"))
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()
        tree = MyTree(retList)

        totalWeight = 0
        for weight in tree.getWeightDict().values():
            totalWeight += weight

        return totalWeight

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()
        tree = MyTree(retList)

        youParent = tree.getParentList('YOU')
        sanParent = tree.getParentList('SAN')

        count = 0
        for youP, sanP in zip(youParent, sanParent):
            if youP != sanP:
                break
            count += 1

        youToCommonParent = len(youParent) - count
        sanToCommonParent = len(sanParent) - count

        return youToCommonParent + sanToCommonParent


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day06(curDir + "/input_short_1.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    uut = Day06(curDir + "/input_short_2.txt")
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day06(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
