import copy
from enum import Enum
from typing import Optional


class IntcodeComputerState_e(Enum):
    NOT_DONE = 0
    DONE = 1
    BREAK_ON_OUTPUT = 2
    BREAK_ON_INPUT = 3


class IntcodeComputer(object):

    def __init__(self, instrList):
        # type: (list[int]) -> None
        self.instrList = copy.copy(instrList)  # type: list[int]
        self.instrPtr = 0  # type: int
        self.inputListIndex = 0  # type: int
        self.inputList = []  # type: list[int]
        self.outputList = []  # type: list[int]
        self.relativeBase = 0  # type: int

    def getInstrList(self):
        # type: () -> list[int]
        return self.instrList

    def clearInput(self):
        # type: () -> None
        self.inputListIndex = 0
        self.inputList.clear()

    def addInput(self, inputVal):
        # type: (int) -> None
        self.inputList.append(inputVal)

    def getCurInputVal(self):
        # type: () -> Optional[int]
        if self.inputListIndex >= len(self.inputList):
            return None
        else:
            inputVal = self.inputList[self.inputListIndex]
            self.inputListIndex += 1
            return inputVal

    def clearOutputList(self):
        # type: () -> None
        self.outputList.clear()

    def getOutputList(self):
        # type: () -> list[int]
        return self.outputList

    def run(self, breakOnOutput=False, askForInput=False, breakOnInput=False):
        # type: (bool, bool, bool) -> IntcodeComputerState_e
        while self.instrPtr < len(self.instrList):
            opcode = self.getOpcode()
            if opcode == 1:  # add
                aVal = self.getValue(1)
                bVal = self.getValue(2)
                result = aVal + bVal
                resultPos = self.getAddress(3)
                self.writeMemory(resultPos, result)
                self.instrPtr += 4
            elif opcode == 2:  # multiply
                aVal = self.getValue(1)
                bVal = self.getValue(2)
                result = aVal * bVal
                resultPos = self.getAddress(3)
                self.writeMemory(resultPos, result)
                self.instrPtr += 4
            elif opcode == 3:  # input
                inputVal = self.getCurInputVal()
                if inputVal is None:
                    if breakOnInput:
                        return IntcodeComputerState_e.BREAK_ON_INPUT
                    if askForInput:
                        while True:
                            userVal = input("Enter input: ")
                            try:
                                inputVal = int(userVal)
                            except ValueError:
                                print("Input {} is not an integer".format(userVal))
                                continue
                            break
                if inputVal is None:
                    raise ValueError("Input value missing")
                inputPos = self.getAddress(1)
                self.writeMemory(inputPos, inputVal)
                self.instrPtr += 2
            elif opcode == 4:  # output
                outputVal = self.getValue(1)
                self.outputList.append(outputVal)
                self.instrPtr += 2
                if breakOnOutput:
                    return IntcodeComputerState_e.BREAK_ON_OUTPUT
            elif opcode == 5:  # jump-if-true
                testVal = self.getValue(1)
                if testVal != 0:
                    self.instrPtr = self.getValue(2)
                else:
                    self.instrPtr += 3
            elif opcode == 6:  # jump-if-false
                testVal = self.getValue(1)
                if testVal == 0:
                    self.instrPtr = self.getValue(2)
                else:
                    self.instrPtr += 3
            elif opcode == 7:  # less than
                aVal = self.getValue(1)
                bVal = self.getValue(2)
                result = 1 if aVal < bVal else 0
                resultPos = self.getAddress(3)
                self.writeMemory(resultPos, result)
                self.instrPtr += 4
            elif opcode == 8:  # equals
                aVal = self.getValue(1)
                bVal = self.getValue(2)
                result = 1 if aVal == bVal else 0
                resultPos = self.getAddress(3)
                self.writeMemory(resultPos, result)
                self.instrPtr += 4
            elif opcode == 9: # adjust relative base
                relativeOffset = self.getValue(1)
                self.relativeBase += relativeOffset
                self.instrPtr += 2
            elif opcode == 99:
                return IntcodeComputerState_e.DONE
            else:
                raise ValueError("Unknown opcode {}".format(opcode))

    def readMemory(self, address):
        # type: (int) -> int
        self.checkAndExtendMemory(address)
        return self.instrList[address]

    def writeMemory(self, address, value):
        # type: (int, int) -> None
        self.checkAndExtendMemory(address)
        self.instrList[address] = value

    def checkAndExtendMemory(self, address):
        # type: (int) -> None
        memorySize = len(self.instrList)
        if address >= memorySize:
            newMemorySize = address + 1
            self.instrList += [0] * (newMemorySize - memorySize)

    def getInstruction(self):
        # type: () -> int
        return self.readMemory(self.instrPtr)

    def getOpcode(self):
        instruction = self.getInstruction()
        return instruction % 100

    def getValue(self, offset):
        # type: (int) -> int
        pos = self.getAddress(offset)
        return self.readMemory(pos)

    def getAddress(self, offset):
        # type: (int) -> int
        instruction = self.getInstruction()
        mode = int(instruction / (10**(1+offset))) % 10
        if mode == 0:
            return self.readMemory(self.instrPtr + offset)
        elif mode == 1:
            return self.instrPtr + offset
        elif mode == 2:
            return self.readMemory(self.instrPtr + offset) + self.relativeBase
        else:
            raise ValueError("Unknown mode {}".format(mode))
