import os
from collections import defaultdict
from typing import Optional

from Year2019.IntcodeComputer.IntcodeComputer import IntcodeComputer, IntcodeComputerState_e


class Day15(object):

    STATUS_HIT_WALL = 0
    STATUS_MOVED = 1
    STATUS_FOUND_OXYGEN = 2

    CMD_NORTH = 1
    CMD_SOUTH = 2
    CMD_WEST = 3
    CMD_EAST = 4
    CMD_SET = {CMD_NORTH, CMD_SOUTH, CMD_EAST, CMD_WEST}

    CHAR_DROID = "D"
    CHAR_WALL = "#"
    CHAR_HALLWAY = "."
    CHAR_UNKNOWN = " "
    CHAR_OXYGEN = "O"

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

        self.curPos = (0, 0)  # type: tuple[int, int]
        self.screenDict = {self.curPos: self.CHAR_DROID}  # type: dict[tuple[int, int], str]
        self.xLim = (0, 0)  # type: tuple[int, int]
        self.yLim = (0, 0)  # type: tuple[int, int]
        self.backTraceList = []  # type: list[int]
        self.triedDict = defaultdict(set)  # type: defaultdict[tuple[int, int], set[int]]
        self.isForward = True  # type: bool
        self.oxygenPos = None  # type: Optional[tuple[int, int]]
        self.longestPath = 0  # type: int

    def parseFile(self):
        # type: () -> list[int]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                curList = line.split(",")
                curList = [int(x) for x in curList]
                retList += curList
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        self.runProgram(retList, True)

        return len(self.backTraceList) - 1

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        self.runProgram(retList, False)

        return self.longestPath - 1

    def runProgram(self, retList, stopOnOxygen):
        # type: (list[int], bool) -> None

        # set manualRun to True to play manually
        manualRun = False
        # set printBoard to True to see the game board
        printBoard = False
        # set printFinalBoard to True to see the game board on exit
        printFinalBoard = True

        computer = IntcodeComputer(retList)
        # initialize inputVal
        inputVal = self.CMD_NORTH  # type: int
        self.initialize(inputVal)
        while True:
            computer.clearOutputList()
            # print screen and current position
            if printBoard:
                self.printState()
                #print("backTraceList", self.backTraceList)
                #print("triedDict", self.triedDict)
                #print("inputVal", inputVal, "isForward", self.isForward)
                #input("Press Enter")
            state = computer.run(breakOnOutput=True, breakOnInput=True)
            if state == IntcodeComputerState_e.DONE:
                break
            elif state == IntcodeComputerState_e.BREAK_ON_INPUT:
                if manualRun:
                    inputVal = self.getManualInputVal()
                computer.clearInput()
                computer.addInput(inputVal)
            elif state == IntcodeComputerState_e.BREAK_ON_OUTPUT:
                outputList = computer.getOutputList()
                status = outputList[0]
                nextPos = self.getNextPos(inputVal)
                if not manualRun:
                    # figure out the next inputVal
                    inputVal = self.getAutomatedInputVal(status, inputVal, nextPos)
                    computer.clearInput()
                    computer.addInput(inputVal)
                self.updateCurPosAndScreeDict(status, nextPos)
                self.updateMax(nextPos)
                if status == self.STATUS_FOUND_OXYGEN:
                    savePos = self.curPos
                    if (self.oxygenPos is None) and (not stopOnOxygen):
                        # clear everything up and start the search over to find the longest path
                        inputVal = self.CMD_NORTH  # type: int
                        self.initialize(inputVal)
                        computer.clearInput()
                        computer.addInput(inputVal)
                    self.oxygenPos = savePos
                    if stopOnOxygen:
                        break
                elif not self.backTraceList:
                    break

        self.screenDict[self.oxygenPos] = self.CHAR_OXYGEN
        if printFinalBoard:
            self.printState()

    def initialize(self, inputVal):
        # type: (int) -> None
        self.curPos = (0, 0)
        self.screenDict = {self.curPos: self.CHAR_DROID}
        self.xLim = (0, 0)
        self.yLim = (0, 0)
        # this is the list of values to go back
        self.backTraceList = [self.getNextBackVal(inputVal)]
        # this keeps a set of values of each point that has been tried already
        self.triedDict = defaultdict(set)
        self.triedDict[self.curPos] = {inputVal}
        self.isForward = True
        self.oxygenPos = None
        self.longestPath = 0

    def printState(self):
        # type: () -> None
        print("Current Position: {}".format(self.curPos))
        print("Current Board:")
        for y in range(self.yLim[1], self.yLim[0]-1, -1):
            curRow = ""
            for x in range(self.xLim[0], self.xLim[1]+1):
                curTile = self.screenDict.get((x, y))
                if curTile is None:
                    curRow += self.CHAR_UNKNOWN
                else:
                    curRow += curTile
            print(curRow)

    def getManualInputVal(self):
        # type: () -> int
        inputVal = self.CMD_NORTH
        while True:
            userVal = input("Enter input: ")
            try:
                inputVal = int(userVal)
            except ValueError:
                print("Input {} is not an integer".format(userVal))
                continue
            if inputVal not in self.CMD_SET:
                print("Input {} must be in {}".format(inputVal, self.CMD_SET))
                continue
            break
        return inputVal

    def getAutomatedInputVal(self, status, inputVal, nextPos):
        # type: (int, int, tuple[int, int]) -> int
        if self.isForward:
            self.triedDict[self.curPos].add(inputVal)
            if status == self.STATUS_HIT_WALL:
                if len(self.triedDict[self.curPos]) == 4:
                    inputVal = self.backTraceList.pop()
                    self.isForward = False
                    return inputVal
                inputVal = self.getNextUntriedInputVal(inputVal, self.triedDict[self.curPos])
                return inputVal
            elif status in [self.STATUS_MOVED, self.STATUS_FOUND_OXYGEN]:
                backVal = self.getNextBackVal(inputVal)
                self.backTraceList.append(backVal)
                if len(self.backTraceList) > self.longestPath:
                    self.longestPath = len(self.backTraceList)
                self.triedDict[nextPos].add(backVal)
                inputVal = self.getNextUntriedInputVal(self.CMD_NORTH, self.triedDict[nextPos])
                return inputVal
            else:
                raise ValueError("Unexpected status {}".format(status))
        else:
            if status in [self.STATUS_MOVED, self.STATUS_FOUND_OXYGEN]:
                if len(self.triedDict[nextPos]) == 4:
                    inputVal = self.backTraceList.pop()
                    return inputVal
                inputVal = self.getNextUntriedInputVal(self.CMD_NORTH, self.triedDict[nextPos])
                self.isForward = True
                return inputVal
            else:
                raise ValueError("Unexpected status {}".format(status))

    def getNextUntriedInputVal(self, inputVal, triedSet):
        # type: (int, set[int]) -> int
        while inputVal in triedSet:
            inputVal = self.getNextInputVal(inputVal)
        return inputVal

    def getNextInputVal(self, inputVal):
        # type: (int) -> int
        if inputVal == self.CMD_EAST:
            return self.CMD_NORTH
        else:
            return inputVal + 1

    def getNextBackVal(self, inputVal):
        # type: (int) -> int
        if inputVal == self.CMD_NORTH:
            return self.CMD_SOUTH
        elif inputVal == self.CMD_SOUTH:
            return self.CMD_NORTH
        elif inputVal == self.CMD_EAST:
            return self.CMD_WEST
        elif inputVal == self.CMD_WEST:
            return self.CMD_EAST
        else:
            raise ValueError("Unknown input {}".format(inputVal))

    def getNextPos(self, inputVal):
        # type: (int) -> tuple[int, int]
        if inputVal == self.CMD_NORTH:
            return self.curPos[0], self.curPos[1]+1
        elif inputVal == self.CMD_EAST:
            return self.curPos[0]+1, self.curPos[1]
        elif inputVal == self.CMD_SOUTH:
            return self.curPos[0], self.curPos[1]-1
        elif inputVal == self.CMD_WEST:
            return self.curPos[0]-1, self.curPos[1]
        else:
            raise ValueError("Unknown input {}".format(inputVal))

    def updateCurPosAndScreeDict(self, status, nextPos):
        # type: (int, tuple[int, int]) -> None
        if status == self.STATUS_HIT_WALL:
            self.screenDict[nextPos] = self.CHAR_WALL
        elif status in [self.STATUS_MOVED, self.STATUS_FOUND_OXYGEN]:
            self.screenDict[nextPos] = self.CHAR_DROID
            self.screenDict[self.curPos] = self.CHAR_HALLWAY
            self.curPos = nextPos
        else:
            raise ValueError("Unknown status {}".format(status))

    def updateMax(self, nextPos):
        # type: (tuple[int, int]) -> None

        minX = min(self.xLim[0], self.curPos[0], nextPos[0])
        maxX = max(self.xLim[1], self.curPos[0], nextPos[0])
        self.xLim = (minX, maxX)
        minY = min(self.yLim[0], self.curPos[1], nextPos[1])
        maxY = max(self.yLim[1], self.curPos[1], nextPos[1])
        self.yLim = (minY, maxY)


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day15(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
