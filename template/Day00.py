import os


class Day00(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str
        self.retList = []  # type: list[str]

    def parseFile(self):
        # type: () -> None
        self.retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                self.retList.append(strippedLine)

    def solve1(self):
        # type: () -> int
        self.parseFile()

        return 0

    def solve2(self):
        # type: () -> int
        self.parseFile()

        return 0


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day00(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day00(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
