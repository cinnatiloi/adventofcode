import copy
import os


class Day08(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile(self):
        # type: () -> list[list[str]]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                retList.append(strippedLine.split(" "))
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        (accumulator, _) = self.runProgram(retList)

        return accumulator

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        isDone = False

        index = 0
        while not isDone:
            instrList = copy.deepcopy(retList)
            if instrList[index][0] == "nop":
                instrList[index][0] = "jmp"
            elif instrList[index][0] == "jmp":
                instrList[index][0] = "nop"
            else:
                index += 1
                continue
            (accumulator, isDone) = self.runProgram(instrList)
            index += 1

        return accumulator

    @staticmethod
    def runProgram(instrList):
        # type: (list[list[str]]) -> (int, bool)

        accumulator = 0
        ptr = 0

        ptrSet = set()

        while True:
            ptrSet.add(ptr)
            if ptr >= len(instrList):
                isDone = True
                break
            instr = instrList[ptr]
            cmd = instr[0]
            val = int(instr[1])
            if cmd == "nop":
                ptr += 1
            elif cmd == "acc":
                accumulator += val
                ptr += 1
            elif cmd == "jmp":
                ptr += val
            else:
                raise ValueError("Unknown cmd {}".format(cmd))
            if ptr in ptrSet:
                isDone = False
                break

        return accumulator, isDone


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day08(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day08(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
