import os


class Day07(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile(self):
        # type: () -> dict[str, dict[str, int]]
        retDict = {}
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                fields = strippedLine.split("contain")
                index = fields[0].find("bag")
                bagColor = fields[0][:index].strip()
                containStr = fields[1].strip()
                containDict = {}
                if containStr[:2] != "no":
                    index = 0
                    while True:
                        numBagsIndex = containStr.find(" ", index)
                        numBags = int(containStr[index:numBagsIndex])
                        colorIdx = containStr.find("bag", numBagsIndex)
                        color = containStr[numBagsIndex:colorIdx].strip()
                        containDict[color] = numBags
                        if containStr.find("bag", colorIdx+3) < 0:
                            break
                        index = containStr.find(" ", colorIdx+3) + 1
                retDict[bagColor] = containDict
        return retDict

    def solve1(self):
        # type: () -> int
        retDict = self.parseFile()

        targetBagColor = "shiny gold"
        numReachable = 0
        for curBagColor in retDict.keys():
            # skip itself
            if curBagColor == targetBagColor:
                continue
            if self.canReach(retDict, curBagColor, targetBagColor):
                numReachable += 1

        return numReachable

    def solve2(self):
        # type: () -> int
        retDict = self.parseFile()

        targetBagColor = "shiny gold"
        return self.totalBags(retDict, targetBagColor)

    @staticmethod
    def canReach(retDict, curBagColor, targetBag):
        # type: (dict[str, dict[str, int]], str, str) -> bool
        if targetBag in retDict[curBagColor].keys():
            return True
        for bagColor in retDict[curBagColor].keys():
            if Day07.canReach(retDict, bagColor, targetBag):
                return True
        return False

    @staticmethod
    def totalBags(retDict, curBagColor):
        # type: (dict[str, dict[str, int]], str) -> int
        if not retDict[curBagColor]:
            return 0
        totalNum = 0
        for bagColor, bagCount in retDict[curBagColor].items():
            totalNum += bagCount + bagCount * Day07.totalBags(retDict, bagColor)
        return totalNum


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day07(curDir + "/input_short_0.txt")
    print("Sample 0 Part 1: {}".format(uut.solve1()))
    print("Sample 0 Part 2: {}".format(uut.solve2()))
    uut = Day07(curDir + "/input_short_1.txt")
    print("Sample 1 Part 2: {}".format(uut.solve2()))
    uut = Day07(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
