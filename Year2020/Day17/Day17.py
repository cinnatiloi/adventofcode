import os


class Day17(object):

    ACTIVE_CHAR = "#"
    INACTIVE_CHAR = "."

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile(self):
        # type: () -> dict[tuple[int, int], str]
        retDict = {}
        with open(self.filename, 'r') as fid:
            col = 0
            for line in fid:
                strippedLine = line.strip()
                for row, state in enumerate(strippedLine):
                    retDict[(row, col)] = state
                col += 1
        return retDict

    def solve1(self):
        # type: () -> int
        retDict = self.parseFile()

        # add 3rd state
        newDict = {}
        for loc, state in retDict.items():
            newDict[(loc[0], loc[1], 0)] = state
        retDict = newDict

        for cycle in range(6):
            newDict = {}
            # add extra layer
            for loc, state in retDict.items():
                newDict[loc] = state
                for x in [-1, 0, 1]:
                    for y in [-1, 0, 1]:
                        for z in [-1, 0, 1]:
                            checkLoc = (loc[0] + x, loc[1] + y, loc[2] + z)
                            if checkLoc not in newDict:
                                newDict[checkLoc] = self.INACTIVE_CHAR
            retDict = newDict
            newDict = {}
            for loc, state in retDict.items():
                numNeighbourActive = 0
                for x in [-1, 0, 1]:
                    for y in [-1, 0, 1]:
                        for z in [-1, 0, 1]:
                            if x == y == z == 0:
                                continue
                            checkLoc = (loc[0]+x, loc[1]+y, loc[2]+z)
                            if (checkLoc in retDict) and retDict[checkLoc] == self.ACTIVE_CHAR:
                                numNeighbourActive += 1
                newDict[loc] = self.getNewState(state, numNeighbourActive)
            retDict = newDict

        numActive = self.countActive(retDict)

        return numActive

    def solve2(self):
        # type: () -> int
        retDict = self.parseFile()

        # add 4th state
        newDict = {}
        for loc, state in retDict.items():
            newDict[(loc[0], loc[1], 0, 0)] = state
        retDict = newDict

        for cycle in range(6):
            newDict = {}
            # add extra layer
            for loc, state in retDict.items():
                newDict[loc] = state
                for x in [-1, 0, 1]:
                    for y in [-1, 0, 1]:
                        for z in [-1, 0, 1]:
                            for w in [-1, 0, 1]:
                                checkLoc = (loc[0] + x, loc[1] + y, loc[2] + z, loc[3] + w)
                                if checkLoc not in newDict:
                                    newDict[checkLoc] = self.INACTIVE_CHAR
            retDict = newDict
            newDict = {}
            for loc, state in retDict.items():
                numNeighbourActive = 0
                for x in [-1, 0, 1]:
                    for y in [-1, 0, 1]:
                        for z in [-1, 0, 1]:
                            for w in [-1, 0, 1]:
                                if x == y == z == w == 0:
                                    continue
                                checkLoc = (loc[0]+x, loc[1]+y, loc[2]+z, loc[3]+w)
                                if (checkLoc in retDict) and retDict[checkLoc] == self.ACTIVE_CHAR:
                                    numNeighbourActive += 1
                newDict[loc] = self.getNewState(state, numNeighbourActive)
            retDict = newDict

        numActive = self.countActive(retDict)

        return numActive

    def getNewState(self, state, numNeighbourActive):
        # type: (str, int) -> str
        if state == self.ACTIVE_CHAR:
            if numNeighbourActive in [2, 3]:
                newState = self.ACTIVE_CHAR
            else:
                newState = self.INACTIVE_CHAR
        else:
            if numNeighbourActive == 3:
                newState = self.ACTIVE_CHAR
            else:
                newState = self.INACTIVE_CHAR
        return newState

    def countActive(self, retDict):
        # type: (dict[tuple, str]) -> int
        numActive = 0
        for state in retDict.values():
            if state == self.ACTIVE_CHAR:
                numActive += 1
        return numActive


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day17(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day17(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
