import os


class Day21(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str
        self.foodList = []  # type: list[list[str]]
        self.allergensDict = {}  # type: dict[str, set[str]]
        self.allergenList = [] # type: list[str]
        self.ingredientWithAllergenList = []  # type: list[str]

    def parseFile(self):
        # type: () -> None
        self.foodList = []
        self.allergensDict = {}
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                isIngredient = True
                curFood = []
                for field in strippedLine.split(" "):
                    if "(contains" in field:
                        isIngredient = False
                        continue
                    if isIngredient:
                        curFood.append(field)
                    else:
                        allergen = field[:-1]
                        if allergen not in self.allergensDict:
                            self.allergensDict[allergen] = set(curFood)
                        else:
                            self.allergensDict[allergen] = self.allergensDict[allergen] & set(curFood)
                self.foodList.append(curFood)

    def solve1(self):
        # type: () -> int
        self.parseFile()

        #print(self.foodList)
        #print(self.allergensDict)

        self.reduceAllergenDict()

        #print(self.allergensDict)

        self.generateIngredientWithAllergenList()

        #print(self.ingredientWithAllergenList)

        total = 0
        for curFood in self.foodList:
            for ingredient in curFood:
                if ingredient not in self.ingredientWithAllergenList:
                    total += 1

        return total

    def solve2(self):
        # type: () -> str
        self.parseFile()

        #print(self.foodList)
        #print(self.allergensDict)

        self.reduceAllergenDict()

        #print(self.allergensDict)

        self.generateIngredientWithAllergenList()

        #print(self.ingredientWithAllergenList)
        #print(self.allergenList)

        sortIdx = [i[0] for i in sorted(enumerate(self.allergenList), key=lambda x: x[1])]

        sortedIngredientList = []
        sortedAllergenList = []
        for idx in sortIdx:
            sortedIngredientList.append(self.ingredientWithAllergenList[idx])
            sortedAllergenList.append(self.allergenList[idx])

        #print(sortedIngredientList)
        #print(sortedAllergenList)

        return ",".join(sortedIngredientList)

    def reduceAllergenDict(self):
        checkedAllergenSet = set()
        while not self.allAllergensFound():
            selectedAllergen = None
            selectedIngredient = None
            for allergen, ingredientSet in self.allergensDict.items():
                if len(ingredientSet) == 1 and allergen not in checkedAllergenSet:
                    selectedAllergen = allergen
                    selectedIngredient = list(ingredientSet)[0]
                    checkedAllergenSet.add(allergen)
                    break
            if selectedAllergen is not None and selectedIngredient is not None:
                for allergen, ingredientSet in self.allergensDict.items():
                    if allergen != selectedAllergen and selectedIngredient in ingredientSet:
                        ingredientSet.discard(selectedIngredient)
                # print(self.allergensDict)
                # print(checkedAllergenSet)
                # input("got here")

    def allAllergensFound(self):
        # type: () -> bool
        for ingredientSet in self.allergensDict.values():
            if len(ingredientSet) != 1:
                return False
        return True

    def generateIngredientWithAllergenList(self):
        self.allergenList = []
        self.ingredientWithAllergenList = []
        for allergen, ingredientSet in self.allergensDict.items():
            self.allergenList.append(allergen)
            self.ingredientWithAllergenList += list(ingredientSet)


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day21(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day21(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
