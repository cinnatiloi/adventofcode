import os


class Day24(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str
        self.gridSet = set()  # type: set[tuple[int, int]]
        self.range0 = (0, 0)  # type: tuple[int, int]
        self.range1 = (0, 0)  # type: tuple[int, int]

    def parseFile(self):
        # type: () -> None
        self.gridSet = set()
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                idx = 0
                curLoc = (0, 0)
                while idx < len(strippedLine):
                    next1 = strippedLine[idx:idx+1]
                    next2 = strippedLine[idx:idx+2]
                    if next2 == "ne":
                        curLoc = (curLoc[0] + 1, curLoc[1] - 1)
                        idx += 2
                    elif next2 == "sw":
                        curLoc = (curLoc[0] - 1, curLoc[1] + 1)
                        idx += 2
                    elif next2 == "nw":
                        curLoc = (curLoc[0], curLoc[1] - 1)
                        idx += 2
                    elif next2 == "se":
                        curLoc = (curLoc[0], curLoc[1] + 1)
                        idx += 2
                    elif next1 == "e":
                        curLoc = (curLoc[0] + 1, curLoc[1])
                        idx += 1
                    elif next1 == "w":
                        curLoc = (curLoc[0] - 1, curLoc[1])
                        idx += 1
                    else:
                        raise ValueError("Unknown next {} {}".format(next1, next2))
                if curLoc in self.gridSet:
                    self.gridSet.remove(curLoc)
                else:
                    self.gridSet.add(curLoc)
                    self.updateRanges(curLoc)
                #print(curLoc)
                #print(self.gridSet)

    def solve1(self):
        # type: () -> int
        self.parseFile()
        #print(self.gridSet)

        numBlackTiles = self.getNumBlackTiles()

        return numBlackTiles

    def solve2(self):
        # type: () -> int
        self.parseFile()
        #print(self.gridSet)
        #print(self.range0)
        #print(self.range1)

        for dayNum in range(100):
            flipList = []
            for x in range(self.range0[0]-1, self.range0[1]+2):
                for y in range(self.range1[0]-1, self.range1[1]+2):
                    curLoc = (x, y)
                    numAdjacentBlack = self.getNumAdjacentBlackTiles(curLoc)
                    if curLoc in self.gridSet:
                        if numAdjacentBlack == 0 or numAdjacentBlack > 2:
                            flipList.append(curLoc)
                    else:
                        if numAdjacentBlack == 2:
                            flipList.append(curLoc)
            for curLoc in flipList:
                if curLoc in self.gridSet:
                    self.gridSet.remove(curLoc)
                else:
                    self.gridSet.add(curLoc)
                    self.updateRanges(curLoc)
            if (dayNum+1) <= 10 or ((dayNum+1) % 10) == 0:
                print("Day {}: {}".format(dayNum+1, self.getNumBlackTiles()))

        return self.getNumBlackTiles()

    def updateRanges(self, curLoc):
        # type: (tuple[int, int]) -> None
        self.range0 = (min(self.range0[0], curLoc[0]), max(self.range0[1], curLoc[0]))
        self.range1 = (min(self.range1[0], curLoc[1]), max(self.range1[1], curLoc[1]))

    def getNumBlackTiles(self):
        # type: () -> int
        return len(self.gridSet)

    def getNumAdjacentBlackTiles(self, curLoc):
        # type: (tuple[int, int]) -> int
        adjacentList = [(1, -1), (-1, 1), (0, 1), (0, -1), (1, 0), (-1, 0)]
        total = 0
        for adj in adjacentList:
            checkLoc = (curLoc[0] + adj[0], curLoc[1] + adj[1])
            if checkLoc in self.gridSet:
                total += 1
        return total


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day24(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day24(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
