import os
from collections import defaultdict
from typing import Optional


class Day18(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str
        self.stackDict = defaultdict(list)  # type: defaultdict[int, list]
        self.stackIdx = 0  # type: int

    def parseFile(self):
        # type: () -> list[list[str]]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                retList.append(strippedLine.split(" "))
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        total = 0
        for line in retList:
            (val, _) = self.calculate(line, 0, False)
            total += val
            #print(val)

        return total

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        total = 0
        for line in retList:
            (val, _) = self.calculate(line, 0, True)
            total += val
            #print(val)

        return total

    def calculate(self, line, start, multLower):
        # type: (list[str], int, bool) -> tuple[int, int]
        val = None
        op = None
        idx = start
        while idx < len(line):
            #print(start, idx, val, op, line, self.stackDict, self.stackIdx)
            s = line[idx]
            closeBracketIdx = s.find(")")
            if s[0] == "(":
                line[idx] = s[1:]
                self.stackIdx += 1
                (_, idx) = self.calculate(line, idx, multLower)
            elif closeBracketIdx >= 0:
                r = int(s[:closeBracketIdx])
                val = self.applyOp(val, r, op)
                val = self.emptyCurrentStack(val)
                self.stackIdx -= 1
                line[idx] = str(val) + s[closeBracketIdx+1:]
                return 0, idx
            if s.isdecimal():
                val = self.applyOp(val, int(s), op)
                idx += 1
            elif s == "+":
                op = s
                idx += 1
            elif s == "*":
                if multLower:
                    self.stackDict[self.stackIdx].append(val)
                    val = None
                    op = None
                else:
                    op = s
                idx += 1
        val = self.emptyCurrentStack(val)
        return val, idx

    def emptyCurrentStack(self, val):
        # type: (int) -> int
        while self.stackDict[self.stackIdx]:
            val *= self.stackDict[self.stackIdx].pop()
        return val

    @staticmethod
    def applyOp(val, newVal, op):
        # type: (Optional[int], int, str) -> int
        if val is None:
            return newVal
        elif op == "+":
            return val + newVal
        elif op == "*":
            return val * newVal
        else:
            raise ValueError("Unknown op {}".format(op))


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day18(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day18(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
