import math
import os


class Day12(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile(self):
        # type: () -> list[tuple[str,int]]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                key = strippedLine[0]
                val = int(strippedLine[1:])
                retList.append((key, val))
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        curPoint = [0, 0]
        curDir = [1, 0]
        self.runNavigation(retList, curPoint, curDir, False)

        return abs(curPoint[0]) + abs(curPoint[1])

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        curPoint = [0, 0]
        curDir = [10, 1]
        self.runNavigation(retList, curPoint, curDir, True)

        return abs(curPoint[0]) + abs(curPoint[1])

    @staticmethod
    def runNavigation(retList, curPoint, curDir, moveDir):
        # type: (list[tuple[str, int]], list[int, int], list[int, int], bool) -> list[int, int]
        for action in retList:
            instr = action[0]
            amount = action[1]
            if instr == "N":
                if moveDir:
                    curDir[1] += amount
                else:
                    curPoint[1] += amount
            elif instr == "S":
                if moveDir:
                    curDir[1] -= amount
                else:
                    curPoint[1] -= amount
            elif instr == "E":
                if moveDir:
                    curDir[0] += amount
                else:
                    curPoint[0] += amount
            elif instr == "W":
                if moveDir:
                    curDir[0] -= amount
                else:
                    curPoint[0] -= amount
            elif instr == "L":
                tmp0 = int(round(curDir[0] * math.cos(2 * math.pi / 360 * amount) - curDir[1] * math.sin(
                    2 * math.pi / 360 * amount)))
                tmp1 = int(round(curDir[0] * math.sin(2 * math.pi / 360 * amount) + curDir[1] * math.cos(
                    2 * math.pi / 360 * amount)))
                curDir = [tmp0, tmp1]
            elif instr == "R":
                tmp0 = int(round(curDir[0] * math.cos(-2 * math.pi / 360 * amount) - curDir[1] * math.sin(
                    -2 * math.pi / 360 * amount)))
                tmp1 = int(round(curDir[0] * math.sin(-2 * math.pi / 360 * amount) + curDir[1] * math.cos(
                    -2 * math.pi / 360 * amount)))
                curDir = [tmp0, tmp1]
            elif instr == "F":
                curPoint[0] += curDir[0] * amount
                curPoint[1] += curDir[1] * amount
            else:
                raise ValueError("Unknown instr {}".format(instr))
        return curPoint


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day12(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day12(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
