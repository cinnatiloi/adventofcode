import os


class Day22(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str
        self.player1 = []  # type: list[int]
        self.player2 = []  # type: list[int]
        self.winningPlayer = []  # type: list[int]

    def parseFile(self):
        # type: () -> None
        self.player1 = []
        self.player2 = []
        with open(self.filename, 'r') as fid:
            isTitle = True
            player = 1
            for line in fid:
                if isTitle:
                    isTitle = False
                    continue
                strippedLine = line.strip()
                if not strippedLine:
                    isTitle = True
                    player += 1
                    continue
                if player == 1:
                    self.player1.append(int(strippedLine))
                else:
                    self.player2.append(int(strippedLine))

    def solve1(self):
        # type: () -> int
        self.parseFile()

        #print(self.player1)
        #print(self.player2)

        self.playGame(list(self.player1), list(self.player2), False)
        winningLen = len(self.winningPlayer)
        #print(self.winningPlayer)
        #print(winningLen)

        total = 0
        for factor, value in zip(range(winningLen, 0, -1), self.winningPlayer):
            total += factor * value

        return total

    def solve2(self):
        # type: () -> int
        self.parseFile()

        #print(self.player1)
        #print(self.player2)

        self.playGame(list(self.player1), list(self.player2), True)
        winningLen = len(self.winningPlayer)
        #print(self.winningPlayer)
        #print(winningLen)

        total = 0
        for factor, value in zip(range(winningLen, 0, -1), self.winningPlayer):
            total += factor * value

        return total

    # returns True is player1 wins
    def playGame(self, player1, player2, playRecursive):
        # type: (list[int], list[int], bool) -> bool
        #print("game start #####################################")
        #print(player1)
        #print(player2)
        player1Played = []
        player2Played = []
        while len(player1) > 0 and len(player2) > 0:
            for p1, p2 in zip(player1Played, player2Played):
                if player1 == p1 and player2 == p2:
                    #print(player1)
                    #print(player2)
                    #input("already played")
                    #self.winningPlayer = player1
                    return True
            player1Played.append(player1)
            player2Played.append(player2)
            #print(player1Played)
            #print(player2Played)
            #input("got here")
            play1Card = player1[0]
            play1RemLen = len(player1)-1
            play2Card = player2[0]
            play2RemLen = len(player2)-1
            if playRecursive and play1RemLen >= play1Card and play2RemLen >= play2Card:
                # play subgame
                player1Win = self.playGame(list(player1[1:play1Card+1]), list(player2[1:play2Card+1]), playRecursive)
            else:
                if play1Card > play2Card:
                    player1Win = True
                elif play1Card < play2Card:
                    player1Win = False
                else:
                    raise ValueError("TIE???")
            if player1Win:
                player1 = player1[1:] + [play1Card, play2Card]
                player2 = player2[1:]
            else:
                player1 = player1[1:]
                player2 = player2[1:] + [play2Card, play1Card]
            #print("round")
            #print(player1)
            #print(player2)
            #input("got here")
        #print(player1)
        #print(player2)
        #print("after win ######################")
        self.winningPlayer = player1 if len(player1) > 0 else player2
        return len(player1) > 0


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day22(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day22(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
