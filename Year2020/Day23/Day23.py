import os


class Day23(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str
        self.numDict = {} # type: dict[int, int]
        self.maxNum = 0 # type: int
        self.firstNum = 0 # type: int
        self.lastNum = 0 # type: int

    def parseFile(self):
        # type: () -> None
        numList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                numList = [int(i) for i in strippedLine]
        self.maxNum = max(numList)
        self.firstNum = numList[0]
        self.lastNum = numList[-1]
        self.numDict = {}
        for idx in range(len(numList)-1):
            self.numDict[numList[idx]] = numList[idx+1]
        self.numDict[self.lastNum] = self.firstNum

    def solve1(self, numMoves):
        # type: (int) -> str
        self.parseFile()

        self.makeMoves(numMoves)

        retStr = ""
        curNum = 1
        while True:
            if self.numDict[curNum] == 1:
                break
            curNum = self.numDict[curNum]
            retStr += str(curNum)

        return retStr

    def solve2(self):
        # type: () -> int
        self.parseFile()

        self.numDict[self.lastNum] = self.maxNum + 1
        newMaxNum = 1000000
        for idx in range(self.maxNum+1, newMaxNum):
            self.numDict[idx] = idx + 1
        self.numDict[newMaxNum] = self.firstNum
        self.maxNum = newMaxNum
        self.lastNum = newMaxNum
        self.makeMoves(10000000)

        num1 = self.numDict[1]
        num2 = self.numDict[num1]
        #print(num1, num2)

        return num1 * num2

    def makeMoves(self, numMoves):
        # type: (int) -> None
        curNum = self.firstNum
        for moveIdx in range(numMoves):
            pickedList = [0, 0, 0]
            curPick = curNum
            for idx in range(3):
                pickedList[idx] = self.numDict[curPick]
                curPick = self.numDict[curPick]
            # update current number
            self.numDict[curNum] = self.numDict[curPick]
            curDest = curNum
            while True:
                curDest -= 1
                if curDest == 0:
                    curDest = self.maxNum
                if curDest not in pickedList:
                    break
            curDestNext = self.numDict[curDest]
            self.numDict[curDest] = pickedList[0]
            self.numDict[pickedList[-1]] = curDestNext
            curNum = self.numDict[curNum]
            #print(self.numDict)


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day23(curDir + "/input_short.txt")
    print("Sample Part 1 10 moves: {}".format(uut.solve1(10)))
    print("Sample Part 1 100 moves: {}".format(uut.solve1(100)))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day23(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1(100)))
    print("Solution Part 2: {}".format(uut.solve2()))
