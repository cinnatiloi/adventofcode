class Day15(object):

    def solve1(self, numList):
        # type: (list[int]) -> int

        return self.findSpoken(numList, 2020)

    def solve2(self, numList):
        # type: (list[int]) -> int

        return self.findSpoken(numList, 30000000)

    @staticmethod
    def findSpoken(numList, numSteps):
        # type: (list[int], int) -> int
        lastSpokenDict = {}
        idx = 1
        for num in numList[:-1]:
            lastSpokenDict[num] = idx
            idx += 1

        lastSpoken = numList[-1]
        while True:
            #print(lastSpoken, idx)
            #print(lastSpokenDict)
            #input()
            if lastSpoken in lastSpokenDict:
                nextSpoken = idx - lastSpokenDict[lastSpoken]
                lastSpokenDict[lastSpoken] = idx
            else:
                lastSpokenDict[lastSpoken] = idx
                nextSpoken = 0
            idx += 1
            lastSpoken = nextSpoken
            if idx == numSteps:
                break

        return lastSpoken


if __name__ == "__main__":
    uut = Day15()
    print("Sample 0 Part 1: {}".format(uut.solve1([0, 3, 6])))
    print("Sample 1 Part 1: {}".format(uut.solve1([1, 3, 2])))
    print("Sample 2 Part 1: {}".format(uut.solve1([2, 1, 3])))
    print("Sample 3 Part 1: {}".format(uut.solve1([1, 2, 3])))
    print("Sample 4 Part 1: {}".format(uut.solve1([2, 3, 1])))
    print("Sample 5 Part 1: {}".format(uut.solve1([3, 2, 1])))
    print("Sample 6 Part 1: {}".format(uut.solve1([3, 1, 2])))
    print("Solution Part 1: {}".format(uut.solve1([20, 0, 1, 11, 6, 3])))
    print("Solution Part 2: {}".format(uut.solve2([20, 0, 1, 11, 6, 3])))
