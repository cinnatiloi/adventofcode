import os


class Day10(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile(self):
        # type: () -> list[int]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                retList.append(int(strippedLine))
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        sortedList = sorted(retList)
        sortedList = [0] + sortedList + [sortedList[-1]+3]
        diffList = []
        for i in range(1, len(sortedList)):
            diffList.append(sortedList[i] - sortedList[i-1])

        print(sortedList)
        print(diffList)

        count1 = diffList.count(1)
        count3 = diffList.count(3)

        return count1 * count3

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        sortedList = sorted(retList)
        sortedList = [0] + sortedList + [sortedList[-1]+3]
        diffList = []
        for i in range(1, len(sortedList)):
            diffList.append(sortedList[i] - sortedList[i-1])

        count = 0
        result = 1
        for diff in diffList:
            if diff == 1:
                count += 1
            elif diff == 3:
                result *= 1 + sum(range(count))
                count = 0
            else:
                raise ValueError("Unknown diff {}".format(diff))

        return result


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    for i in range(2):
        uut = Day10(curDir + "/input_short_{}.txt".format(i))
        print("Sample {} Part 1: {}".format(i, uut.solve1()))
        print("Sample {} Part 2: {}".format(i, uut.solve2()))
    uut = Day10(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
