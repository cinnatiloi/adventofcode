import os


class Day09(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile(self):
        # type: () -> list[int]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                retList.append(int(strippedLine))
        return retList

    def solve1(self, preambleLen):
        # type: (int) -> int
        retList = self.parseFile()

        badIdx = None
        for idx in range(preambleLen,len(retList)):
            isValid = False
            for i in range(1,preambleLen+1):
                for j in range(1,preambleLen+1):
                    if retList[idx] == (retList[idx-i] + retList[idx-j]):
                        isValid = True
                        break
                if isValid:
                    break
            if not isValid:
                badIdx = idx
                break

        if isValid:
            raise ValueError("List is valid")

        return retList[badIdx]

    def solve2(self, preambleLen):
        # type: (int) -> int

        badNum = self.solve1(preambleLen)

        retList = self.parseFile()
        for idx in range(len(retList)):
            total = 0
            numFound = False
            minVal = None
            maxVal = None
            for i in range(idx, len(retList)):
                if (maxVal is None) or (retList[i] > maxVal):
                    maxVal = retList[i]
                if (minVal is None) or (retList[i] < minVal):
                    minVal = retList[i]
                total += retList[i]
                if total == badNum:
                    numFound = True
                    break
                elif total > badNum:
                    break
            if numFound:
                return minVal + maxVal

        raise ValueError("No consecutive sum found")


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day09(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1(5)))
    print("Sample Part 2: {}".format(uut.solve2(5)))
    uut = Day09(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1(25)))
    print("Solution Part 2: {}".format(uut.solve2(25)))
