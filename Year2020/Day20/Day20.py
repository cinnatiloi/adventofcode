import math
import os
from typing import Optional


class Day20(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str
        self.tileDict = {}  # type: dict[int, list[str]]
        self.monsterNumRows = 0  # type: int
        self.monsterNumCols = 0  # type: int
        self.picture = []  # type: list[str]

    def parseFile(self):
        # type: () -> dict[int, dict[str, str]]
        edgeDict = {}
        self.tileDict = {}
        with open(self.filename, 'r') as fid:
            rowNum = 0
            top = ""
            bottom = ""
            left = ""
            right = ""
            tileNum = None
            for line in fid:
                strippedLine = line.strip()
                if not strippedLine:
                    edgeDict[tileNum] = {
                        "top": top,
                        "bottom": bottom,
                        "left": left,
                        "right": right
                    }
                    rowNum = 0
                    top = ""
                    bottom = ""
                    left = ""
                    right = ""
                    tileNum = None
                    continue
                if tileNum is None:
                    tileNum = int(strippedLine[5:-1])
                    continue
                left = strippedLine[0] + left
                right += strippedLine[-1]
                if rowNum == 0:
                    top = strippedLine
                    self.tileDict[tileNum] = []
                if rowNum == len(strippedLine)-1:
                    bottom = strippedLine[::-1]
                if rowNum not in [0, len(strippedLine)-1]:
                    self.tileDict[tileNum].append(strippedLine[1:-1])
                rowNum += 1
            edgeDict[tileNum] = {
                "top": top,
                "bottom": bottom,
                "left": left,
                "right": right
            }
        return edgeDict

    def parseMonster(self):
        # type: () -> list[tuple[int, int]]
        retList = []
        curDir = os.path.dirname(os.path.abspath(__file__))
        filename = curDir + "/monster.txt"
        self.monsterNumCols = 0
        with open(filename, 'r') as fid:
            row = 0
            for line in fid:
                for col, c in enumerate(line):
                    if c == "#":
                        retList.append((row, col))
                        if col > self.monsterNumCols:
                            self.monsterNumCols = col
                row += 1
        self.monsterNumRows = row
        self.monsterNumCols += 1
        return retList

    def solve1(self):
        # type: () -> int
        edgeDict = self.parseFile()

        #print(edgeDict)

        matchDict = self.getMatchDict(edgeDict)

        #print(matchDict)

        cornerList = self.getCornerList(matchDict)

        #print(cornerList)

        res = 1
        for tileNum in cornerList:
            res *= tileNum

        return res

    def solve2(self):
        # type: () -> int
        edgeDict = self.parseFile()

        #print(edgeDict)
        #print(self.tileDict)

        matchDict = self.getMatchDict(edgeDict)

        #print(matchDict)

        cornerList = self.getCornerList(matchDict)

        #print(cornerList)

        pictureDim = int(math.sqrt(len(edgeDict)))

        self.picture = []
        curTileNum = cornerList[0]
        #print("first tile")
        #print(curTileNum)
        #print(matchDict[curTileNum])
        #print(self.tileDict[curTileNum])
        self.fixFirstTile(matchDict, curTileNum)
        #print("after fix first tile")
        #print(matchDict[curTileNum])
        #print(self.tileDict[curTileNum])
        for row in range(pictureDim):
            newRow = True
            rowFirstTileNum = curTileNum
            for col in range(pictureDim):
                self.addToPicture(curTileNum, newRow)
                newRow = False
                #print("before fix right")
                #print(curTileNum)
                #print(matchDict[curTileNum])
                #print(self.tileDict[curTileNum])
                curTileNum = self.fixRightTile(matchDict, curTileNum)
                #print("after fix right")
                #print(curTileNum)
                #if curTileNum is not None:
                #    print(matchDict[curTileNum])
                #    print(self.tileDict[curTileNum])
                #input("got here")
            #print("before fix bottom")
            #print(rowFirstTileNum)
            #print(matchDict[rowFirstTileNum])
            #print(self.tileDict[rowFirstTileNum])
            curTileNum = self.fixBottomTile(matchDict, rowFirstTileNum)
            #print("after fix bottom")
            #print(curTileNum)
            #if curTileNum is not None:
            #    print(matchDict[curTileNum])
            #    print(self.tileDict[curTileNum])
            #input("after fix bottom")

        #self.printPicture()
        #print(matchDict)

        numHashesInPicture = self.findNumHashesInPicture()
        numMonstersHashes = self.findNumHashesInMonsters()

        return numHashesInPicture - numMonstersHashes

    def getMatchDict(self, edgeDict):
        # type: (dict[int, dict[str, str]]) -> dict[int, dict[str,tuple[int, str, bool]]]
        matchDict = {}
        for refTileNum, refEdgeDict in edgeDict.items():
            tileMatchDict = {
                "top": None,
                "bottom": None,
                "left": None,
                "right": None,
            }
            for compTileNum, compEdgeDict in edgeDict.items():
                if refTileNum == compTileNum:
                    continue
                for refDir, refEdge in refEdgeDict.items():
                    for compDir, compEdge in compEdgeDict.items():
                        matchMode = self.edgeMatch(refEdge, compEdge)
                        if matchMode is not None:
                            if tileMatchDict[refDir] is not None:
                                raise ValueError("Already found match")
                            tileMatchDict[refDir] = (compTileNum, compDir, matchMode)
            matchDict[refTileNum] = tileMatchDict
        return matchDict

    def getCornerList(self, matchDict):
        cornerList = []
        for tileNum, tileMatchDict in matchDict.items():
            numNoMatchEdge = 0
            for match in tileMatchDict.values():
                if match is None:
                    numNoMatchEdge += 1
            if numNoMatchEdge == 2:
                cornerList.append(tileNum)
        return cornerList

    def rotateTileRight(self, matchDict, tileNum):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int) -> None
        #print("rotate right")
        tile = self.tileDict[tileNum]
        newTile = []
        for col in range(len(tile[0])):
            newStr = ""
            for row in range(len(tile)-1, -1, -1):
                newStr += tile[row][col]
            newTile.append(newStr)
        self.tileDict[tileNum] = newTile
        self.rotateMatchDictRight(matchDict, tileNum)

    def rotateTileLeft(self, matchDict, tileNum):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int) -> None
        #print("rotate left")
        tile = self.tileDict[tileNum]
        newTile = []
        for col in range(len(tile[0])-1, -1, -1):
            newStr = ""
            for row in range(len(tile)):
                newStr += tile[row][col]
            newTile.append(newStr)
        self.tileDict[tileNum] = newTile
        self.rotateMatchDictLeft(matchDict, tileNum)

    def flipTileUpDown(self, matchDict, tileNum):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int) -> None
        #print("flip up down")
        tile = self.tileDict[tileNum]
        newTile = []
        for row in range(len(tile)-1, -1, -1):
            newStr = ""
            for col in range(len(tile[0])):
                newStr += tile[row][col]
            newTile.append(newStr)
        self.tileDict[tileNum] = newTile
        self.flipMatchDictUpDown(matchDict, tileNum)

    def flipTileLeftRight(self, matchDict, tileNum):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int) -> None
        #print("flip left right")
        tile = self.tileDict[tileNum]
        newTile = []
        for row in range(len(tile)):
            newStr = ""
            for col in range(len(tile[0])-1, -1, -1):
                newStr += tile[row][col]
            newTile.append(newStr)
        self.tileDict[tileNum] = newTile
        self.flipMatchDictLeftRight(matchDict, tileNum)

    def addToPicture(self, tileNum, newRow):
        # type: (int, bool) -> None
        tile = self.tileDict[tileNum]
        if newRow:
            self.picture += tile
        else:
            startRow = len(self.picture) - len(tile)
            for row, line in enumerate(tile):
                self.picture[startRow+row] += line

    def printPicture(self):
        # type: () -> None
        for line in self.picture:
            print(line)

    # flips the corner such that it's the top left
    def fixFirstTile(self, matchDict, tileNum):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int) -> None
        tileMatchDict = dict(matchDict[tileNum])
        for direction, match in tileMatchDict.items():
            if match is not None:
                #if direction == "top":
                #    self.flipTileUpDown(matchDict, tileNum)
                if direction in ["top", "bottom"] and match[2] is False:
                    self.flipTileLeftRight(matchDict, tileNum)
                #if direction == "left":
                #    self.flipTileLeftRight(matchDict, tileNum)
                if direction in ["left", "right"] and match[2] is False:
                    self.flipTileUpDown(matchDict, tileNum)
        tileMatchDict = dict(matchDict[tileNum])
        emptyDirSet = set()
        for direction, match in tileMatchDict.items():
            if match is None:
                emptyDirSet.add(direction)
        if emptyDirSet == {"bottom", "left"}:
            self.rotateTileRight(matchDict, tileNum)
        elif emptyDirSet == {"right", "bottom"}:
            self.rotateTileRight(matchDict, tileNum)
            self.rotateTileRight(matchDict, tileNum)
        elif emptyDirSet == {"top", "right"}:
            self.rotateTileLeft(matchDict, tileNum)

    def fixRightTile(self, matchDict, tileNum):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int) -> Optional[int]
        rightTile = matchDict[tileNum]['right']
        if rightTile is None:
            return None
        newTileNum = rightTile[0]
        #print(newTileNum)
        #print(matchDict[newTileNum])
        #print(self.tileDict[newTileNum])
        newTileMatchDict = dict(matchDict[newTileNum])
        for direction, match in newTileMatchDict.items():
            if match is not None and match[0] == tileNum:
                if direction == 'right':
                    if match[2]:
                        self.rotateTileRight(matchDict, newTileNum)
                        self.rotateTileRight(matchDict, newTileNum)
                    else:
                        self.flipTileLeftRight(matchDict, newTileNum)
                elif direction == 'left':
                    if not match[2]:
                        self.flipTileUpDown(matchDict, newTileNum)
                elif direction == 'top':
                    if match[2]:
                        self.rotateTileLeft(matchDict, newTileNum)
                    else:
                        self.rotateTileLeft(matchDict, newTileNum)
                        self.flipTileUpDown(matchDict, newTileNum)
                elif direction == 'bottom':
                    if match[2]:
                        self.rotateTileRight(matchDict, newTileNum)
                    else:
                        self.rotateTileRight(matchDict, newTileNum)
                        self.flipTileUpDown(matchDict, newTileNum)
        return newTileNum

    def fixBottomTile(self, matchDict, tileNum):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int) -> Optional[int]
        bottomTile = matchDict[tileNum]['bottom']
        if bottomTile is None:
            return None
        newTileNum = bottomTile[0]
        #print(newTileNum)
        #print(matchDict[newTileNum])
        #print(self.tileDict[newTileNum])
        newTileMatchDict = dict(matchDict[newTileNum])
        for direction, match in newTileMatchDict.items():
            if match is not None and match[0] == tileNum:
                if direction == 'bottom':
                    if match[2]:
                        self.rotateTileRight(matchDict, newTileNum)
                        self.rotateTileRight(matchDict, newTileNum)
                    else:
                        self.flipTileUpDown(matchDict, newTileNum)
                elif direction == 'top':
                    if not match[2]:
                        self.flipTileLeftRight(matchDict, newTileNum)
                elif direction == 'left':
                    if match[2]:
                        self.rotateTileRight(matchDict, newTileNum)
                    else:
                        self.rotateTileRight(matchDict, newTileNum)
                        self.flipTileLeftRight(matchDict, newTileNum)
                elif direction == 'right':
                    if match[2]:
                        self.rotateTileLeft(matchDict, newTileNum)
                    else:
                        self.rotateTileLeft(matchDict, newTileNum)
                        self.flipTileLeftRight(matchDict, newTileNum)
        return newTileNum

    def findNumHashesInMonsters(self):
        # type: () -> int
        monster = self.parseMonster()
        #print(monster)
        #print(len(self.picture), len(self.picture[0]), self.monsterNumRows, self.monsterNumCols)

        numRotates = 0
        numFlips = 0
        monstersSet = set()
        while True:
            #print(numRotates, numFlips)
            #self.printPicture()
            for rowStart in range(len(self.picture)-self.monsterNumRows+1):
                for colStart in range(len(self.picture[0])-self.monsterNumCols+1):
                    foundMonster = True
                    curMonster = []
                    for loc in monster:
                        row = rowStart + loc[0]
                        col = colStart + loc[1]
                        curMonster.append((row, col))
                        #if numRotates == 1 and numFlips == 1:
                        #    print(rowStart, colStart, row, col, self.picture[row][col])
                        if self.picture[row][col] != "#":
                            foundMonster = False
                            #if numRotates == 1 and numFlips == 1:
                            #    input("no match")
                            break
                        #if numRotates == 1 and numFlips == 1:
                        #    input("continue")
                    if foundMonster:
                        for loc in curMonster:
                            monstersSet.add(loc)
            self.rotatePictureRight()
            numRotates += 1
            if numRotates == 4:
                numRotates = 0
                self.flipPictureUpDown()
                numFlips += 1
            if numFlips == 2:
                break

        #print("len(monsterSet)", len(monstersSet))

        return len(monstersSet)

    def rotatePictureRight(self):
        # type: () -> None
        newPicture = []
        for col in range(len(self.picture[0])):
            newStr = ""
            for row in range(len(self.picture)-1, -1, -1):
                newStr += self.picture[row][col]
            newPicture.append(newStr)
        self.picture = newPicture

    def flipPictureUpDown(self):
        # type: () -> None
        newPicture = []
        for row in range(len(self.picture)-1, -1, -1):
            newStr = ""
            for col in range(len(self.picture[0])):
                newStr += self.picture[row][col]
            newPicture.append(newStr)
        self.picture = newPicture

    def findNumHashesInPicture(self):
        # type: () -> int
        numHashes = 0
        for row in self.picture:
            numHashes += row.count("#")
        return numHashes

    @staticmethod
    def rotateMatchDictRight(matchDict, tileNum):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int) -> None
        #print(tileNum)
        #print(matchDict[tileNum])
        newTileMatchDict = {
            "top": Day20.getNewMatchAndRotateRight(matchDict, tileNum, "left"),
            "bottom": Day20.getNewMatchAndRotateRight(matchDict, tileNum, "right"),
            "left": Day20.getNewMatchAndRotateRight(matchDict, tileNum, "bottom"),
            "right": Day20.getNewMatchAndRotateRight(matchDict, tileNum, "top")
        }
        matchDict[tileNum] = newTileMatchDict
        #print(tileNum)
        #print(matchDict[tileNum])
        #input("got here first")

    @staticmethod
    def getNewMatchAndRotateRight(matchDict, tileNum, direction):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int, str) -> tuple[int, str, bool]
        #print(direction)
        tileMatchDict = matchDict[tileNum]
        match = tileMatchDict[direction]
        if match is not None:
            matchTileNum = match[0]
            matchDir = match[1]
            matchMatchMode = match[2]
            match = (matchTileNum, matchDir, matchMatchMode)
            #print(matchTileNum)
            #print(matchDict[matchTileNum])
            dirMatch = matchDict[matchTileNum][matchDir]
            if dirMatch is not None:
                matchDict[matchTileNum][matchDir] = (dirMatch[0], Day20.turnRight(direction), dirMatch[2])
            #print(matchDict[matchTileNum])
        return match

    @staticmethod
    def turnRight(direction):
        # type: (str) -> str
        if direction == "left":
            return "top"
        if direction == "top":
            return "right"
        if direction == "right":
            return "bottom"
        if direction == "bottom":
            return "left"

    @staticmethod
    def rotateMatchDictLeft(matchDict, tileNum):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int) -> None
        newTileMatchDict = {
            "top": Day20.getNewMatchAndRotateLeft(matchDict, tileNum, "right"),
            "bottom": Day20.getNewMatchAndRotateLeft(matchDict, tileNum, "left"),
            "left": Day20.getNewMatchAndRotateLeft(matchDict, tileNum, "top"),
            "right": Day20.getNewMatchAndRotateLeft(matchDict, tileNum, "bottom")
        }
        matchDict[tileNum] = newTileMatchDict

    @staticmethod
    def getNewMatchAndRotateLeft(matchDict, tileNum, direction):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int, str) -> tuple[int, str, bool]
        #print(direction)
        tileMatchDict = matchDict[tileNum]
        match = tileMatchDict[direction]
        if match is not None:
            matchTileNum = match[0]
            matchDir = match[1]
            matchMatchMode = match[2]
            match = (matchTileNum, matchDir, matchMatchMode)
            #print(matchTileNum)
            #print(matchDict[matchTileNum])
            dirMatch = matchDict[matchTileNum][matchDir]
            if dirMatch is not None:
                matchDict[matchTileNum][matchDir] = (dirMatch[0], Day20.turnLeft(direction), dirMatch[2])
            #print(matchDict[matchTileNum])
        return match

    @staticmethod
    def turnLeft(direction):
        # type: (str) -> str
        if direction == "left":
            return "bottom"
        if direction == "top":
            return "left"
        if direction == "right":
            return "top"
        if direction == "bottom":
            return "right"

    @staticmethod
    def flipMatchDictUpDown(matchDict, tileNum):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int) -> None
        newTileMatchDict = {
            "top": Day20.getNewMatchAndFlipDir(matchDict, tileNum, "bottom"),
            "bottom": Day20.getNewMatchAndFlipDir(matchDict, tileNum, "top"),
            "left": Day20.getNewMatchAndFlipMode(matchDict, tileNum, "left"),
            "right": Day20.getNewMatchAndFlipMode(matchDict, tileNum, "right")
        }
        matchDict[tileNum] = newTileMatchDict

    @staticmethod
    def flipMatchDictLeftRight(matchDict, tileNum):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int) -> None
        newTileMatchDict = {
            "top": Day20.getNewMatchAndFlipMode(matchDict, tileNum, "top"),
            "bottom": Day20.getNewMatchAndFlipMode(matchDict, tileNum, "bottom"),
            "left": Day20.getNewMatchAndFlipDir(matchDict, tileNum, "right"),
            "right": Day20.getNewMatchAndFlipDir(matchDict, tileNum, "left")
        }
        matchDict[tileNum] = newTileMatchDict

    @staticmethod
    def getNewMatchAndFlipMode(matchDict, tileNum, direction):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int, str) -> tuple[int, str, bool]
        tileMatchDict = matchDict[tileNum]
        match = tileMatchDict[direction]
        if match is not None:
            matchTileNum = match[0]
            matchDir = match[1]
            matchMatchMode = not match[2]
            match = (matchTileNum, matchDir, matchMatchMode)
            dirMatch = matchDict[matchTileNum][matchDir]
            if dirMatch is not None:
                matchDict[matchTileNum][matchDir] = (dirMatch[0], dirMatch[1], not dirMatch[2])
        return match

    @staticmethod
    def getNewMatchAndFlipDir(matchDict, tileNum, direction):
        # type: (dict[int, dict[str, tuple[int, str, bool]]], int, str) -> tuple[int, str, bool]
        tileMatchDict = matchDict[tileNum]
        match = tileMatchDict[direction]
        if match is not None:
            matchTileNum = match[0]
            matchDir = match[1]
            matchMatchMode = not match[2]
            match = (matchTileNum, matchDir, matchMatchMode)
            dirMatch = matchDict[matchTileNum][matchDir]
            if dirMatch is not None:
                matchDict[matchTileNum][matchDir] = (dirMatch[0], Day20.flipDir(direction), not dirMatch[2])
        return match

    @staticmethod
    def flipDir(direction):
        # type: (str) -> str
        if direction == "left":
            return "right"
        if direction == "top":
            return "bottom"
        if direction == "right":
            return "left"
        if direction == "bottom":
            return "top"

    @staticmethod
    def edgeMatch(edge1, edge2):
        # type: (str, str) -> Optional[bool]
        edgeRev2 = edge2[::-1]
        if edge1 == edge2:
            return False
        elif edge1 == edgeRev2:
            return True
        else:
            return None


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day20(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day20(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
