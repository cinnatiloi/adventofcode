import copy
import os


class Day11(object):

    EMPTY = "L"
    FLOOR = "."
    OCCUPIED = "#"

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile(self):
        # type: () -> list[list[str]]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                retList.append(list(strippedLine))
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        emptyThresh = 4
        checkLimit = 1

        numOccupied = self.getSteadyStateNumOccupied(retList, emptyThresh, checkLimit)

        return numOccupied

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        emptyThresh = 5
        checkLimit = max(len(retList), len(retList[0]))

        numOccupied = self.getSteadyStateNumOccupied(retList, emptyThresh, checkLimit)

        return numOccupied

    def getSteadyStateNumOccupied(self, retList, emptyThresh, checkLimit):
        # type: (list[list[str]], int, int) -> int

        numRows = len(retList)
        numCols = len(retList[0])
        newBoard = copy.deepcopy(retList)
        while True:
            hasChanged = False
            numOccupied = 0
            for i in range(numRows):
                for j in range(numCols):
                    (newBoard[i][j], curChanged) = self.getNewState(retList, i, j, numRows, numCols, emptyThresh,
                                                                    checkLimit)
                    if curChanged:
                        hasChanged = True
                    if newBoard[i][j] == self.OCCUPIED:
                        numOccupied += 1
            tmp = newBoard
            newBoard = retList
            retList = tmp
            if not hasChanged:
                break
        return numOccupied

    def getNewState(self, retList, i, j, numRows, numCols, emptyThresh, checkLimit):
        # type: (list[list[str]], int, int, int, int, int, int) -> tuple[str, bool]
        curState = retList[i][j]
        numNeighboursOccupied = self.getImmedateNeighboursOccupied(retList, i, j, numRows, numCols,
                                                                   checkLimit)
        if numNeighboursOccupied == 0 and curState == self.EMPTY:
            return self.OCCUPIED, True
        if numNeighboursOccupied >= emptyThresh and curState == self.OCCUPIED:
            return self.EMPTY, True
        return curState, False

    def getImmedateNeighboursOccupied(self, retList, i, j, numRows, numCols, checkLimit):
        # type: (list[list[str]], int, int, int, int, int) -> int
        occupiedList = [None] * 8
        for lim in range(1, checkLimit+1):
            xList = [-lim, 0, lim, -lim, lim, -lim, 0, lim]
            yList = [lim, lim, lim, 0, 0, -lim, -lim, -lim]
            for idx, (x, y) in enumerate(zip(xList, yList)):
                if occupiedList[idx] is not None:
                    continue
                ii = i + x
                jj = j + y
                if ii < 0 or ii >= numRows:
                    continue
                if jj < 0 or jj >= numCols:
                    continue
                if retList[ii][jj] == self.OCCUPIED:
                    occupiedList[idx] = 1
                elif retList[ii][jj] == self.EMPTY:
                    occupiedList[idx] = 0
            if None not in occupiedList:
                break
        for idx in range(len(occupiedList)):
            if occupiedList[idx] is None:
                occupiedList[idx] = 0
        return sum(occupiedList)


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day11(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day11(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
