import os


class Day25(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str
        self.retList = []  # type: list[int]

    def parseFile(self):
        # type: () -> None
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                self.retList.append(int(strippedLine))

    def solve1(self):
        # type: () -> int
        self.parseFile()

        secretKeys = [None, None]
        val = 1
        count = 0
        while True:
            val *= 7
            if val >= 20201227:
                val %= 20201227
            count += 1
            if secretKeys[0] is None and val == self.retList[0]:
                secretKeys[0] = count
            if secretKeys[1] is None and val == self.retList[1]:
                secretKeys[1] = count
            if secretKeys[0] is not None and secretKeys[1] is not None:
                break

        #print(secretKeys)
        #print(self.retList)

        val = 1
        for idx in range(secretKeys[1]):
            val *= self.retList[0]
            val %= 20201227

        return val

    def solve2(self):
        # type: () -> int
        self.parseFile()

        return 0


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day25(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day25(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
