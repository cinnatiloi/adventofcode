import os


class Day03(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename

    def parseFile(self):
        # type: () -> list[str]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                retList.append(line.strip())
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        numTrees = 0
        index = 0
        for retStr in retList:
            if retStr[index] == "#":
                numTrees += 1
            index += 3
            index %= len(retStr)
        return numTrees

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        rightList = [1, 3, 5, 7, 1]
        downList = [1, 1, 1, 1, 2]
        prod = 1
        for right, down in zip(rightList, downList):
            numTrees = 0
            index = 0
            downCount = down - 1
            for retStr in retList:
                downCount += 1
                if downCount < down:
                    continue
                downCount = 0
                if retStr[index] == "#":
                    numTrees += 1
                index += right
                index %= len(retStr)
            print('numTrees:{} '.format(numTrees))
            prod *= numTrees
        return prod


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day03(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day03(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
