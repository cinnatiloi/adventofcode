import os


class Day14(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile1(self):
        # type: () -> dict[int, int]
        mem = {}
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                fields = strippedLine.split(" = ")
                if fields[0] == "mask":
                    maskStr = fields[1]
                    zeroMask = 0
                    oneMask = 0
                    val = 2**35
                    for idx in range(36):
                        if maskStr[idx] == "0":
                            zeroMask += val
                        elif maskStr[idx] == "1":
                            oneMask += val
                        val //= 2
                    zeroMask = ~zeroMask
                else:
                    bracketOpen = fields[0].find("[")
                    bracketClose = fields[0].find("]")
                    address = int(fields[0][bracketOpen+1:bracketClose])
                    val = int(fields[1])
                    val |= oneMask
                    val &= zeroMask
                    mem[address] = val
        return mem

    def parseFile2(self):
        # type: () -> dict[int, int]
        mem = {}
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                fields = strippedLine.split(" = ")
                if fields[0] == "mask":
                    maskStr = fields[1]
                    zeroMask = 0
                    oneMask = 0
                    val = 2**35
                    floatingList = []
                    for idx in range(36):
                        if maskStr[idx] == "X":
                            zeroMask += val
                            floatingList.append(35-idx)
                        elif maskStr[idx] == "1":
                            oneMask += val
                        val //= 2
                    zeroMask = ~zeroMask
                    addrIncrList = [0]
                    for floating in floatingList:
                        newAddrIncrList = []
                        for a in addrIncrList:
                            newAddrIncrList.append(a + 2**floating)
                        addrIncrList += newAddrIncrList
                else:
                    bracketOpen = fields[0].find("[")
                    bracketClose = fields[0].find("]")
                    address = int(fields[0][bracketOpen+1:bracketClose])
                    val = int(fields[1])
                    address |= oneMask
                    address &= zeroMask
                    for a in addrIncrList:
                        mem[address + a] = val
        return mem

    def solve1(self):
        # type: () -> int
        mem = self.parseFile1()

        return sum(mem.values())

    def solve2(self):
        # type: () -> int
        mem = self.parseFile2()

        return sum(mem.values())


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day14(curDir + "/input_short.txt")
    print("Sample 0 Part 1: {}".format(uut.solve1()))
    uut = Day14(curDir + "/input_short_1.txt")
    print("Sample 1 Part 2: {}".format(uut.solve2()))
    uut = Day14(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
