import os


class Day04(object):

    REQUIRED_KEYS = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
    OPTIONAL_KEYS = ["cid"]

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename

    def parseFile(self):
        # type: () -> list[dict]
        retList = []
        with open(self.filename, 'r') as fid:
            retDict = {}
            for line in fid:
                if not line.strip():
                    retList.append(retDict)
                    retDict = {}
                    continue
                fields = line.split()
                for c in fields:
                    pair = c.split(':')
                    retDict[pair[0]] = pair[1]
        # add last one
        retList.append(retDict)
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        numValid = 0
        for retDict in retList:
            if self.isBasicValid(retDict):
                numValid +=1

        return numValid

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        numValid = 0
        for retDict in retList:
            if self.isAdvancedValid(retDict):
                numValid += 1

        return numValid

    def isBasicValid(self, retDict):
        # type: (dict) -> bool
        sortedAllKeys = sorted(self.REQUIRED_KEYS + self.OPTIONAL_KEYS)
        sortedRequiredKeys = sorted(self.REQUIRED_KEYS)
        sortedEntryKeys = sorted(retDict.keys())
        return sortedEntryKeys == sortedAllKeys or sortedEntryKeys == sortedRequiredKeys

    def isAdvancedValid(self, retDict):
        # type: (dict) -> bool
        return (self.isBasicValid(retDict) and
                self.isBirthYearValid(retDict) and
                self.isIssueYearValid(retDict) and
                self.isExpirationYearValid(retDict) and
                self.isHeightValid(retDict) and
                self.isHairColorValid(retDict) and
                self.isEyeColorValid(retDict) and
                self.isPassportIDValid(retDict))

    @staticmethod
    def isBirthYearValid(retDict):
        # type: (dict) -> bool
        byr = retDict["byr"]
        return Day04.isIntegerInRange(byr, 1920, 2002)

    @staticmethod
    def isIssueYearValid(retDict):
        # type: (dict) -> bool
        iyr = retDict["iyr"]
        return Day04.isIntegerInRange(iyr, 2010, 2020)

    @staticmethod
    def isExpirationYearValid(retDict):
        # type: (dict) -> bool
        eyr = retDict["eyr"]
        return Day04.isIntegerInRange(eyr, 2020, 2030)

    @staticmethod
    def isHeightValid(retDict):
        # type: (dict) -> bool
        hgt = retDict["hgt"]
        val = hgt[:-2]
        units = hgt[-2:]
        if units == "cm":
            return Day04.isIntegerInRange(val, 150, 193)
        elif units == "in":
            return Day04.isIntegerInRange(val, 59, 76)
        else:
            return False

    @staticmethod
    def isHairColorValid(retDict):
        # type: (dict) -> bool
        hcl = retDict["hcl"]
        if hcl[0] != "#":
            return False
        val = hcl[1:]
        try:
            int(val, 16)
        except ValueError:
            return False
        return True

    @staticmethod
    def isEyeColorValid(retDict):
        # type: (dict) -> bool
        ecl = retDict["ecl"]
        expectedColors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
        return ecl in expectedColors

    @staticmethod
    def isPassportIDValid(retDict):
        # type: (dict) -> bool
        pid = retDict["pid"]
        return pid.isdigit() and (len(pid) == 9)

    @staticmethod
    def isIntegerInRange(val, minVal, maxVal):
        # type: (int, int, int) -> bool
        if not val.isdigit():
            return False
        return minVal <= int(val) <= maxVal


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day04(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day04(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
