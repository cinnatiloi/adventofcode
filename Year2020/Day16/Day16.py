import os
from collections import defaultdict
from typing import Optional


class Day16(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str
        self.keyDict = {}  # type: dict[str, list[int]]
        self.myTicket = []  # type: list[int]
        self.nearbyTickets = []  # type: list[list[int]]

    def parseFile(self):
        # type: () -> None
        self.keyDict = {}
        with open(self.filename, 'r') as fid:
            parsing = 0
            for line in fid:
                strippedLine = line.strip()
                if not strippedLine:
                    parsing += 1
                    continue
                if parsing == 0:
                    fields = strippedLine.split(": ")
                    key = fields[0]
                    numRangeList = fields[1].split(" or ")
                    validList =[]
                    for r in numRangeList:
                        lim = r.split("-")
                        validList += list(range(int(lim[0]), int(lim[1])+1))
                    self.keyDict[key] = validList
                elif parsing == 1:
                    if strippedLine.find(":") >= 0:
                        continue
                    myTicket = strippedLine.split(",")
                    self.myTicket = [int(i) for i in myTicket]
                elif parsing == 2:
                    if strippedLine.find(":") >= 0:
                        self.nearbyTickets = []
                        continue
                    nearbyTicket = strippedLine.split(",")
                    nearbyTicket = [int(i) for i in nearbyTicket]
                    self.nearbyTickets.append(nearbyTicket)

    def solve1(self):
        # type: () -> int
        self.parseFile()

        invalidNumList = []
        for ticket in self.nearbyTickets:
            for num in ticket:
                isValid = False
                for validList in self.keyDict.values():
                    if num in validList:
                        isValid = True
                        break
                if not isValid:
                    invalidNumList.append(num)

        return sum(invalidNumList)

    def solve2(self):
        # type: () -> int
        self.parseFile()

        validTicketList = []
        for ticket in self.nearbyTickets:
            isTicketValid = True
            for num in ticket:
                isValid = False
                for validList in self.keyDict.values():
                    if num in validList:
                        isValid = True
                        break
                if not isValid:
                    isTicketValid = False
                    break
            if isTicketValid:
                validTicketList.append(ticket)

        guessDict = {}
        for idx in range(len(validTicketList[0])):
            guessDict[idx] = set(self.keyDict.keys())

        for ticket in validTicketList:
            for idx, num in enumerate(ticket):
                for key, validList in self.keyDict.items():
                    if num not in validList:
                        guessDict[idx].remove(key)

        doneList = []
        while not self.isAllSingle(guessDict):
            nextSingleIdx = self.findSingleIndex(guessDict, doneList)
            if nextSingleIdx is None:
                break
            for idx, guess in guessDict.items():
                if idx == nextSingleIdx:
                    continue
                guess.discard(list(guessDict[nextSingleIdx])[0])
            doneList.append(nextSingleIdx)

        print(guessDict)

        product = 1
        for idx, num in enumerate(self.myTicket):
            fieldName = list(guessDict[idx])[0]
            if "departure" in fieldName:
                product *= num

        return product

    @staticmethod
    def isAllSingle(guessDict):
        # type: (dict[int, set[str]]) -> bool
        for guess in guessDict.values():
            if len(guess) > 1:
                return False
        return True

    @staticmethod
    def findSingleIndex(guessDict, doneList):
        # type: (dict[int, set[str]], list[int]) -> Optional[int]
        for idx, guess in guessDict.items():
            if (idx not in doneList) and len(guess) == 1:
                return idx
        return None


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day16(curDir + "/input_short_0.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    uut = Day16(curDir + "/input_short_1.txt")
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day16(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
