import os


class Day05(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename

    def parseFile(self):
        # type: () -> list[str]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                retList.append(line)
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        maxNum = 0
        for l in retList:
            curNum = self.strToSeat(l)
            if curNum > maxNum:
                maxNum = curNum
        return maxNum

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        seatList = []
        for l in retList:
            seatList.append(self.strToSeat(l))
        sortedSeatList = sorted(seatList)
        for seat in sortedSeatList:
            if (seat + 1) not in sortedSeatList:
                return seat + 1
        return None

    @staticmethod
    def strToSeat(strVal):
        # type: (str) -> int
        val = strVal.replace("F", "0")
        val = val.replace("B", "1")
        val = val.replace("L", "0")
        val = val.replace("R", "1")
        return int(val, 2)


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day05(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day05(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
