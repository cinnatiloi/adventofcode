import math
import os


class Day13(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str

    def parseFile(self):
        # type: () -> tuple[list[int], list[int], int]
        index = 0
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                if index == 0:
                    departTime = int(strippedLine)
                elif index == 1:
                    tmpBusList = strippedLine.split(",")
                else:
                    raise ValueError("Expected input file. Must only have 2 lines.")
                index += 1
        busList = []
        busIdxList = []
        for idx, bus in enumerate(tmpBusList):
            if bus != "x":
                busList.append(int(bus))
                busIdxList.append(idx)
        return busList, busIdxList, departTime

    def solve1(self):
        # type: () -> int
        (busList, _, departTime) = self.parseFile()

        earliestTime = None
        earliestBus = None
        for bus in busList:
            t = int(math.ceil(departTime / bus)) * bus
            if (earliestTime is None) or (t < earliestTime):
                earliestTime = t
                earliestBus = bus

        #print(departTime, earliestTime, earlistBus)

        return (earliestTime-departTime) * earliestBus

    def solve2(self):
        # type: () -> int
        (busList, busIdxList, departTime) = self.parseFile()

        #print(busList)
        #print(busIdxList)

        curTry = 0
        curIdx = 1
        increment = busList[0]

        while True:
            isFound = True
            for bus, busIdx in zip(busList[:curIdx+1], busIdxList[:curIdx+1]):
                #print(curTry, bus, busIdx)
                if ((curTry+busIdx) % bus) != 0:
                    isFound = False
                    break
            if isFound and curIdx == len(busList)-1:
                break
            if isFound:
                increment *= busList[curIdx]
                curIdx += 1
            else:
                curTry += increment
            #print(curTry, curIdx, increment)
            #input("Press Enter")

        return curTry


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    for idx in range(6):
        uut = Day13(curDir + "/input_short_{}.txt".format(idx))
        if idx == 0:
            print("Sample {} Part 1: {}".format(idx, uut.solve1()))
        print("Sample {} Part 2: {}".format(idx, uut.solve2()))
    uut = Day13(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
