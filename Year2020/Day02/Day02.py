import os


class Day02(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename

    def parseFile(self):
        # type: () -> list[dict]
        retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                fields = line.split(':')
                retDict = {}
                retDict['password'] = fields[1].strip()
                fields = fields[0].split(' ')
                retDict['repChar'] = fields[1]
                fields = fields[0].split('-')
                retDict['minRep'] = int(fields[0])
                retDict['maxRep'] = int(fields[1])
                retList.append(retDict)
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        numValid = 0
        for retDict in retList:
            count = retDict['password'].count(retDict['repChar'])
            if retDict['minRep'] <= count <= retDict['maxRep']:
                numValid += 1
        return numValid

    def solve2(self):
        # type () -> int
        retList = self.parseFile()

        numValid = 0
        for retDict in retList:
            count1 = 0
            count2 = 0
            if retDict['password'][retDict['minRep']-1] == retDict['repChar']:
                count1 = 1
            if retDict['password'][retDict['maxRep']-1] == retDict['repChar']:
                count2 = 1
            if count1 + count2 == 1:
                numValid += 1
        return numValid


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day02(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day02(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
