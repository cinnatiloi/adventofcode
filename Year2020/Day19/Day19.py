import os
from enum import Enum
from typing import Union


class ParseState_e(Enum):
    PARSE_RULES = 0
    PARSE_MSGS = 1


class SearchState_e(Enum):
    SEARCH_42 = 0
    SEARCH_31 = 1


class Day19(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str
        self.knownMap = {}
        self.known42 = []
        self.known31 = []

    def parseFile(self):
        # type: () -> tuple[dict[int, Union[list[list[int]], str]], list[str]]
        retDict = {}
        retList = []
        with open(self.filename, 'r') as fid:
            parseState = ParseState_e.PARSE_RULES
            for line in fid:
                strippedLine = line.strip()
                if not strippedLine:
                    parseState = ParseState_e.PARSE_MSGS
                    continue
                if parseState == ParseState_e.PARSE_RULES:
                    fields = strippedLine.split(": ")
                    key = int(fields[0])
                    if fields[1][0].isdigit():
                        retDict[key] = []
                        numListList = fields[1].split(" | ")
                        for numList in numListList:
                            intList = numList.split(" ")
                            intList = [int(i) for i in intList]
                            retDict[key].append(intList)
                    else:
                        retDict[key] = fields[1][1]
                elif parseState == ParseState_e.PARSE_MSGS:
                    retList.append(strippedLine)
                else:
                    raise ValueError("Unknown state {}".format(parseState))
        return retDict, retList

    def solve1(self, useSpecialCount):
        # type: (bool) -> int
        retDict, retList = self.parseFile()

        #print(retDict)
        #print(retList)

        self.knownMap = {}
        validList = self.buildValidList(retDict, 0)

        #print(validList)

        if useSpecialCount:
            total = self.specialCountTotal(retList, False)
        else:
            total = self.countTotal(retList, validList)


        return total

    def countTotal(self, retList, validList):
        # type: (list[str], list[str]) -> int
        total = 0
        for msg in retList:
            if msg in validList:
                total += 1
        return total

    def solve2(self):
        # type: () -> int
        retDict, retList = self.parseFile()

        #print(retDict)
        #print(retList)
        retDict[8] = [[42], [42, 8]]
        retDict[11] = [[42, 31], [42, 11, 31]]

        self.knownMap = {}
        self.buildValidList(retDict, 0)

        #print(self.known42)
        #print(self.known31)

        total = self.specialCountTotal(retList, True)

        return total

    # specialCountTotal uses the knowledge that 0: 8 11
    # and 8: 42, 11: 42 31, so 0: 42 42 31
    # when hasLoop is True, it is basically a repetition
    # where 0: 42...42 31..31
    def specialCountTotal(self, retList, hasLoop):
        # type: (list[str], bool) -> int
        length42 = len(self.known42[0])
        for i in self.known42:
            if length42 != len(i):
                raise ValueError("Mismatch length")
        length31 = len(self.known31[0])
        for i in self.known31:
            if length31 != len(i):
                raise ValueError("Mismatch length")

        total = 0
        for msg in retList:
            idx = 0
            searchState = SearchState_e.SEARCH_42
            count42 = 0
            count31 = 0
            while idx < len(msg):
                if searchState == SearchState_e.SEARCH_42:
                    if msg[idx:idx + length42] not in self.known42:
                        searchState = SearchState_e.SEARCH_31
                    else:
                        idx += length42
                        count42 += 1
                else:
                    if msg[idx:idx + length31] not in self.known31:
                        break
                    else:
                        idx += length31
                        count31 += 1
            countDiff = count42 - count31
            if hasLoop:
                isValid = idx == len(msg) and count42 > 1 and count31 > 0 and countDiff >= 1
            else:
                isValid = idx == len(msg) and count42 == 2 and count31 == 1
            if isValid:
                total += 1
        return total

    def buildValidList(self, retDict, startVal):
        # type: (dict, int) -> list[str]
        curVal = retDict[startVal]
        if isinstance(curVal, str):
            return [curVal]
        if self.knownMap.get(startVal) is not None:
            return self.knownMap[startVal]
        # control how deep we're going
        strList = []
        for numList in curVal:
            #print("numList start", numList)
            tmpListList = []
            for num in numList:
                if num != startVal:
                    newList = self.buildValidList(retDict, num)
                    tmpListList.append(newList)
                #print("num", num, tmpListList)
                #input("num")
            flatList = self.flattenList(tmpListList)
            strList += flatList
            #print("numList", numList, strList)
            #input("numList")
        if startVal == 42:
            self.known42 = strList
        if startVal == 31:
            self.known31 = strList
        self.knownMap[startVal] = strList
        return strList

    def flattenList(self, tmpListList):
        # type: (list[list[str]]) -> list[str]
        flatList = []
        if len(tmpListList) == 1:
            flatList += tmpListList[0]
        elif len(tmpListList) == 2:
            for idx1, tmpList1 in enumerate(tmpListList):
                for tmpList2 in tmpListList[idx1+1:]:
                    for tmp1 in tmpList1:
                        for tmp2 in tmpList2:
                            flatList.append(tmp1 + tmp2)
        else:
            aList = self.flattenList(tmpListList[:2])
            newListList = [aList] + tmpListList[2:]
            flatList = self.flattenList(newListList)
        return flatList


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day19(curDir + "/input_short_0.txt")
    print("Sample 0 Part 1: {}".format(uut.solve1(False)))
    uut = Day19(curDir + "/input_short_1.txt")
    print("Sample 1 Part 1: {}".format(uut.solve1(True)))
    print("Sample 1 Part 2: {}".format(uut.solve2()))
    uut = Day19(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1(True)))
    print("Solution Part 2: {}".format(uut.solve2()))
