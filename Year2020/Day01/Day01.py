import os


class Day01(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename

    def getNumList(self):
        # type: () -> list[int]
        numList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                numList.append(int(line))
        return numList

    def solve1(self):
        # type: () -> int
        numList = self.getNumList()

        for x in numList:
            for y in numList:
                if (x + y) == 2020:
                    return x * y

    def solve2(self):
        # type: () -> int

        numList = self.getNumList()

        for x in numList:
            for y in numList:
                for z in numList:
                    if (x + y + z) == 2020:
                        return x * y * z


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day01(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day01(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
