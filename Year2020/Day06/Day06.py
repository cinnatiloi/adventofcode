import os


class Day06(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename

    def parseFile(self):
        # type: () -> list[list[str]]
        retList = []
        with open(self.filename, 'r') as fid:
            curGroup = []
            for line in fid:
                strippedLine = line.strip()
                if not strippedLine:
                    retList.append(curGroup)
                    curGroup = []
                    continue
                curGroup.append(strippedLine)
            if curGroup:
                retList.append(curGroup)
        return retList

    def solve1(self):
        # type: () -> int
        retList = self.parseFile()

        numAnswers = 0
        for curGroup in retList:
            answerSet = set()
            for answers in curGroup:
                for idx in range(len(answers)):
                    answerSet.add(answers[idx])
            numAnswers += len(answerSet)

        return numAnswers

    def solve2(self):
        # type: () -> int
        retList = self.parseFile()

        numAnswers = 0
        for curGroup in retList:
            allAnswers = None
            for answers in curGroup:
                if allAnswers is None:
                    allAnswers = set()
                    for idx in range(len(answers)):
                        allAnswers.add(answers[idx])
                else:
                    toDiscard = []
                    for curChar in allAnswers:
                        if curChar not in answers:
                            toDiscard.append(curChar)
                    for curChar in toDiscard:
                        allAnswers.discard(curChar)
            numAnswers += len(allAnswers)

        return numAnswers


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day06(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day06(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
