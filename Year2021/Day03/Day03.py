import os


class Day03(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str
        self.retList = []  # type: list[str]

    def parseFile(self):
        # type: () -> None
        self.retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                self.retList.append(strippedLine)

    def solve1(self):
        # type: () -> int
        self.parseFile()

        weights = [0] * len(self.retList[0])
        for binNum in self.retList:
            for idx, b in enumerate(binNum):
                if b == '0':
                    weights[idx] -= 1
                elif b == '1':
                    weights[idx] += 1
                else:
                    raise ValueError("Unknown bit {}".format(b))

        gammaRateBin = ""
        epsilonRateBin = ""
        for w in weights:
            if w > 0:
                gammaRateBin += '1'
                epsilonRateBin += '0'
            elif w < 0:
                gammaRateBin += '0'
                epsilonRateBin += '1'
            else:
                raise ValueError("weight is 0")

        gammaRate = int(gammaRateBin, 2)
        epsilonRate = int(epsilonRateBin, 2)

        print("Part 1 gamma rate is {} and epsilon rate is {}".format(gammaRate, epsilonRate))

        return gammaRate * epsilonRate

    def solve2(self):
        # type: () -> int
        self.parseFile()

        oxygenIdxList = range(len(self.retList))
        co2IdxList = range(len(self.retList))
        for idx in range(len(self.retList[0])):
            if len(oxygenIdxList) > 1:
                zeroList = []
                oneList = []
                weight = 0
                for numIdx in oxygenIdxList:
                    if self.retList[numIdx][idx] == '0':
                        weight -= 1
                        zeroList.append(numIdx)
                    elif self.retList[numIdx][idx] == '1':
                        weight += 1
                        oneList.append(numIdx)
                    else:
                        raise ValueError("Unknown bit {}".format(self.retList[numIdx][idx]))
                if weight >= 0:
                    oxygenIdxList = oneList
                else:
                    oxygenIdxList = zeroList
            if len(co2IdxList) > 1:
                zeroList = []
                oneList = []
                weight = 0
                for numIdx in co2IdxList:
                    if self.retList[numIdx][idx] == '0':
                        weight -= 1
                        zeroList.append(numIdx)
                    elif self.retList[numIdx][idx] == '1':
                        weight += 1
                        oneList.append(numIdx)
                    else:
                        raise ValueError("Unknown bit {}".format(self.retList[numIdx][idx]))
                if weight >= 0:
                   co2IdxList = zeroList
                else:
                    co2IdxList = oneList
            if len(oxygenIdxList) == 1 and len(co2IdxList) == 1:
                break

        oxygen = int(self.retList[oxygenIdxList[0]], 2)
        co2 = int(self.retList[co2IdxList[0]], 2)

        print("Part 2 oxygen is {} and co2 is {}".format(oxygen, co2))

        return oxygen * co2


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day03(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day03(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
