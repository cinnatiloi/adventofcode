import os


class Day01(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str
        self.retList = []  # type: list[int]

    def parseFile(self):
        # type: () -> None
        self.retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                self.retList.append(int(strippedLine))

    def solve1(self):
        # type: () -> int
        self.parseFile()

        incrCount = 0
        for idx, depth in enumerate(self.retList):
            if idx == 0:
                continue
            if depth > self.retList[idx-1]:
                incrCount += 1

        return incrCount

    def solve2(self):
        # type: () -> int
        self.parseFile()

        incrCount = 0
        curSum = 0
        for idx, depth in enumerate(self.retList):
            if idx < 3:
                curSum += depth
                continue
            lastSum = curSum
            # calculate a running average by adding the new depth and subtracting the oldest depth
            curSum += depth - self.retList[idx-3]
            if curSum > lastSum:
                incrCount += 1

        return incrCount


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day01(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day01(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
