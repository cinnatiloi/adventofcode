import os


class Day02(object):

    def __init__(self, filename):
        # type: (str) -> None
        self.filename = filename  # type: str
        self.retList = []  # type: list[str]

    def parseFile(self):
        # type: () -> None
        self.retList = []
        with open(self.filename, 'r') as fid:
            for line in fid:
                strippedLine = line.strip()
                self.retList.append(strippedLine)

    def solve1(self):
        # type: () -> int
        self.parseFile()

        # horizontal, depth
        curPos = [0, 0]
        for instr in self.retList:
            (direction, moves) = instr.split(' ')
            if direction == "forward":
                curPos[0] += int(moves)
            elif direction == "up":
                curPos[1] -= int(moves)
            elif direction == "down":
                curPos[1] += int(moves)
            else:
                raise ValueError("Unknown direction " + direction)

        print("Part 1 final position {}, {}".format(curPos[0], curPos[1]))

        return curPos[0] * curPos[1]

    def solve2(self):
        # type: () -> int
        self.parseFile()

        # horizontal, depth, aim
        curPos = [0, 0, 0]
        for instr in self.retList:
            (direction, moves) = instr.split(' ')
            if direction == "forward":
                curPos[0] += int(moves)
                curPos[1] += curPos[2] * int(moves)
            elif direction == "up":
                curPos[2] -= int(moves)
            elif direction == "down":
                curPos[2] += int(moves)
            else:
                raise ValueError("Unknown direction " + direction)

        print("Part 2 final position {}, {}".format(curPos[0], curPos[1]))

        return curPos[0] * curPos[1]


if __name__ == "__main__":
    curDir = os.path.dirname(os.path.abspath(__file__))
    uut = Day02(curDir + "/input_short.txt")
    print("Sample Part 1: {}".format(uut.solve1()))
    print("Sample Part 2: {}".format(uut.solve2()))
    uut = Day02(curDir + "/input.txt")
    print("Solution Part 1: {}".format(uut.solve1()))
    print("Solution Part 2: {}".format(uut.solve2()))
